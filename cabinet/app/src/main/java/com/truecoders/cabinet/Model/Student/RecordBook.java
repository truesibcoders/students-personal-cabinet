package com.truecoders.cabinet.Model.Student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.realm.RealmList;
import io.realm.RealmObject;

public class RecordBook extends RealmObject {

    @SerializedName("semesterList")
    @Expose
    private RealmList<Semester> semesterList = null;

    public RealmList<Semester> getSemesterList() {
        return semesterList;
    }
}