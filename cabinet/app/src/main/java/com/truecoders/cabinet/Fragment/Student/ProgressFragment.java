package com.truecoders.cabinet.Fragment.Student;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import com.james602152002.floatinglabelspinner.FloatingLabelSpinner;
import com.truecoders.cabinet.Adapter.SpinnerAdapter.SemesterSpinnerAdapter;
import com.truecoders.cabinet.Model.General.BaseClass;
import com.truecoders.cabinet.Model.Student.ECoursesList;
import com.truecoders.cabinet.Model.Student.Profile;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.RetroClient;
import com.truecoders.expandingview.ExpandingList;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

import static com.truecoders.cabinet.Adapter.ListViewAdapter.Student.RatingListAdapter.renderProgressView;
import static com.truecoders.cabinet.DAO.Student.RatingDAO.addProgressToRealm;

public class ProgressFragment extends Fragment {
    private List<String> spinnerList = new ArrayList<>();
    private FloatingLabelSpinner spinner;
    private ProgressBar progressBar;
    private List<ECoursesList> eCoursesList;
    private ExpandingList expandingList;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.subfragment_progress, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinner = view.findViewById(R.id.spinner);

        progressBar = view.findViewById(R.id.spin_kit);
        expandingList = view.findViewById(R.id.expanding_list_main);

        Profile profile = Realm.getDefaultInstance().where(Profile.class).findFirst();
        eCoursesList = Realm.getDefaultInstance().where(ECoursesList.class).findAll();

        spinner.setOnItemSelectedListener(spinnerOnItemSelected);
        spinner.setDropDownHintView(View.inflate(getContext(), R.layout.spinner_header, null));

        if (eCoursesList.size() > 0) {
            showSpinner(eCoursesList);
        } else if (profile != null) {
            RetroClient.getApiService().getProgress(
                    profile.getUsername(),
                    profile.getPassword()
            ).enqueue(new Callback<BaseClass>() {
                @Override
                public void onResponse(@NonNull Call<BaseClass> call, @NonNull Response<BaseClass> response) {
                    if (response.body() != null) {
                        eCoursesList = response.body().getECoursesList();
                        addProgressToRealm(eCoursesList);
                        showSpinner(eCoursesList);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<BaseClass> call, @NonNull Throwable t) {

                }
            });
        }
    }

    private void showSpinner(List<ECoursesList> eCoursesList) {
        for (ECoursesList eCourse : eCoursesList) {
            if (eCourse.getECourseName().length() > 35)
                spinnerList.add(eCourse.getECourseName().substring(0, 30) + "...");
            else
                spinnerList.add(eCourse.getECourseName());
        }

        progressBar.setVisibility(View.GONE);

        spinner.setVisibility(View.VISIBLE);
        spinner.setAdapter(new SemesterSpinnerAdapter(getContext(), spinnerList));
    }

    private AdapterView.OnItemSelectedListener spinnerOnItemSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position != 0) {
                expandingList.removeAllViews();

                renderProgressView(expandingList, eCoursesList.get(position - 1).getECourses());

                AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setRepeatCount(0);
                anim.setDuration(500);
                anim.setRepeatMode(Animation.REVERSE);
                expandingList.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                expandingList.startAnimation(anim);
                spinner.dismiss();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
}