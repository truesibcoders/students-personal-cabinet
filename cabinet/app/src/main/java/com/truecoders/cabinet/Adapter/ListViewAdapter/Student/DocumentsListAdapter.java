package com.truecoders.cabinet.Adapter.ListViewAdapter.Student;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.truecoders.cabinet.Model.Student.Document;
import com.truecoders.cabinet.R;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * @author Ilya Baykalov
 */

public class DocumentsListAdapter extends ArrayAdapter<Document> {
    private LayoutInflater inflater;
    private List<Document> newsList;

    public DocumentsListAdapter(Context context, List<Document> newsList) {
        super(context, R.layout.adapter_document, newsList);
        this.newsList = newsList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.adapter_document, parent, false);
        }

        final Document document = newsList.get(position);

        String status = "";
        switch (document.getIdFSS()) {
            case 0:
                status = "Создана";
                break;
            case 1:
                status = "Одобрена";
                break;
            case 2:
                status = "Отклонена";
                break;
            default:
                break;
        }
        ((TextView) view.findViewById(R.id.tv_type_order)).setText(String.format("%s", document.getIdTFS() == 1 ? "По месту требования" : "Пенсионный фонд"));
        ((TextView) view.findViewById(R.id.tv_value_date_start_order)).setText(String.format("%s",
                new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(document.getCreateDate())));
        ((TextView) view.findViewById(R.id.tv_value_status_order)).setText(status);

        if (document.getDoneDate() != null) {
            ((TextView) view.findViewById(R.id.tv_value_date_end_order)).setText(String.format("%s",
                    new SimpleDateFormat("dd.MM.yyyy",Locale.getDefault()).format(document.getDoneDate())));
        } else {
            view.findViewById(R.id.tv_value_date_end_order).setVisibility(View.GONE);
            view.findViewById(R.id.tv_date_end_order).setVisibility(View.GONE);
        }
        view.findViewById(R.id.tv_cancel_order).setOnClickListener(v -> {

        });

        return view;
    }


}