package com.truecoders.cabinet.DAO.Entrant;

import com.truecoders.cabinet.Model.Entrant.Contact;
import io.realm.Realm;

/**
 * @author Maria Fabrichkina
 */

public class ContactsDAO {

    public static void addContactToRealm(Contact contact) {

        Realm.getDefaultInstance().executeTransaction(realm -> {
            Contact realmObject = realm.createObject(Contact.class);

            realmObject.setTitle(contact.getTitle());
            realmObject.setPhone(contact.getPhone());
            realmObject.setAddress(contact.getAddress());
            realmObject.setEmail(contact.getEmail());
            realmObject.setLinkTitle(contact.getLinkTitle());
            realmObject.setLink(contact.getLink());
        });
    }
}
