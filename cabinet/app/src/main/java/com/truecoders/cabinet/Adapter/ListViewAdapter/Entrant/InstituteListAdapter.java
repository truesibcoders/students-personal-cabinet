package com.truecoders.cabinet.Adapter.ListViewAdapter.Entrant;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.truecoders.cabinet.Activity.Entrant.InstituteDescriptionActivity;
import com.truecoders.cabinet.Model.Entrant.Institute;
import com.truecoders.cabinet.R;

import java.util.List;
import java.util.Objects;

/**
 * @author Maria Fabrichkina
 */

public class InstituteListAdapter extends ArrayAdapter<Institute> {
    private LayoutInflater inflater;
    private List<Institute> institutes;

    public InstituteListAdapter(Context context, List<Institute> institutes) {
        super(context, R.layout.adapter_institute, institutes);
        this.institutes = institutes;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.adapter_institute, parent, false);
        }

        ((TextView) view.findViewById(R.id.institute_title)).setText(String.format("%s", institutes.get(position).getTitle()));
        ((TextView) view.findViewById(R.id.institute_abbreviation)).setText(String.format("%s", institutes.get(position).getAbbreviation()));

        if (!institutes.get(position).getImage().equals("null")) {
            Picasso.get()
                    .load(institutes.get(position).getImage())
                    .placeholder(R.drawable.ic_sfu_logo_24dp)
                    .into(((ImageView) view.findViewById(R.id.background_image_institute)));
        } else {
            ((ImageView) view.findViewById(R.id.background_image_institute)).setImageResource(R.drawable.ic_sfu_logo_24dp);
        }

        view.findViewById(R.id.btn_more_info).setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), InstituteDescriptionActivity.class);
            Institute institute = new Institute();

            institute.setTitle(Objects.requireNonNull(institutes.get(position)).getTitle());
            institute.setAbbreviation(Objects.requireNonNull(institutes.get(position)).getAbbreviation());
            institute.setAddress(Objects.requireNonNull(institutes.get(position)).getAddress());
            institute.setOperatingModeWeekday(Objects.requireNonNull(institutes.get(position)).getOperatingModeWeekday());
            institute.setOperatingModeSat(Objects.requireNonNull(institutes.get(position)).getOperatingModeSat());
            institute.setOperatingModeSun(Objects.requireNonNull(institutes.get(position)).getOperatingModeSun());
            institute.setContactName(Objects.requireNonNull(institutes.get(position)).getContactName());
            institute.setPhone(Objects.requireNonNull(institutes.get(position)).getPhone());
            institute.setEmail(Objects.requireNonNull(institutes.get(position)).getEmail());
            institute.setImage(Objects.requireNonNull(institutes.get(position)).getImage());

            intent.putExtra("institute", institute);
            getContext().startActivity(intent);
        });
        return view;
    }
}