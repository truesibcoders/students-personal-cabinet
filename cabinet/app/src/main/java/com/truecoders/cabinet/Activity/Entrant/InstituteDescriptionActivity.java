package com.truecoders.cabinet.Activity.Entrant;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.truecoders.cabinet.Fragment.Entrant.InstituteImageZoomingFragment;
import com.truecoders.cabinet.Fragment.Entrant.MapEntrantFragment;
import com.truecoders.cabinet.Model.Entrant.Institute;
import com.truecoders.cabinet.R;

/**
 * @author Maria Fabrichkina
 */

public class InstituteDescriptionActivity extends AppCompatActivity {
    Fragment fragment;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_institute_description);

        Institute institute = (Institute) getIntent().getSerializableExtra("institute");

        ((TextView) findViewById(R.id.tvGeneral)).setText(institute.getTitle());
        ((TextView) findViewById(R.id.tvAddress)).setText(institute.getAddress());
        ((TextView) findViewById(R.id.tvWeekday)).setText(institute.getOperatingModeWeekday());
        ((TextView) findViewById(R.id.tvSaturday)).setText(institute.getOperatingModeSat());
        ((TextView) findViewById(R.id.tvSunday)).setText(institute.getOperatingModeSun());
        ((TextView) findViewById(R.id.tvContactName)).setText(institute.getContactName());
        ((TextView) findViewById(R.id.tvNumber)).setText(institute.getPhone());
        ((TextView) findViewById(R.id.tvEmail)).setText(institute.getEmail());

        ImageView image = findViewById(R.id.ivImage);
        Picasso.get().load(institute.getImage()).into(image);
        image.setScaleType(ImageView.ScaleType.CENTER_CROP);


        image.setOnClickListener(v -> {
            fragment = new InstituteImageZoomingFragment();

            Bundle bundle = new Bundle();
            bundle.putString("image", institute.getImage());
            fragment.setArguments(bundle);

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame_inst, fragment, "tag");
            ft.addToBackStack(null);
            ft.commit();
        });
    }
}
