package com.truecoders.cabinet.Activity.Entrant;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.truecoders.cabinet.Fragment.Entrant.HostelsFacilitiesFragment;
import com.truecoders.cabinet.Fragment.Entrant.InterviewWithResidentOfHostelFragment;
import com.truecoders.cabinet.Fragment.Entrant.MapEntrantFragment;
import com.truecoders.cabinet.Model.Entrant.Hostel;
import com.truecoders.cabinet.Model.General.Image;
import com.truecoders.cabinet.R;
import io.realm.Realm;

import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Maria Fabrichkina
 */

public class HostelDescriptionActivity extends AppCompatActivity {

    CarouselView carouselView;
    CollapsingToolbarLayout collapsingToolbarLayout;
    private TextView tvCurrentNumber, tvCountOfImages, tvHostelTitle, tvHostelAddress, tvPrice,
            tvTypeOfHostels, tvCountOfNeighbors, tvLaundry, tvWC, tvShower, tvNameOfCommand,
            tvPhoneNumberOfCommand;

    private List<Image> listOfImage;
    private double latitude, longitude;
    private ImageView tvMapMarker;
    private Button btnInterview, btnAllFacilities;

    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hostel_description);

        Intent intent = getIntent();
        int position = intent.getExtras().getInt("position");

        List<Hostel> hostelList = Realm.getDefaultInstance().where(Hostel.class).findAll();
        final Hostel hostel = hostelList.get(position);

        listOfImage = hostel.getImages();
        String[] images = new String[listOfImage.size()];

        for (int i = 0; i < listOfImage.size(); i++) {
            images[i] = hostel.getImages().get(i).getImage();
        }

        tvCurrentNumber = findViewById(R.id.tvCurrentNumber);
        tvCountOfImages = findViewById(R.id.tvCount);
        tvHostelTitle = findViewById(R.id.tvHostelTitle);
        tvHostelAddress = findViewById(R.id.tvHostelAddress);
        tvPrice = findViewById(R.id.tvPrice);
        tvTypeOfHostels = findViewById(R.id.tvTypeOfHostels);
        tvCountOfNeighbors = findViewById(R.id.tvRoominess);
        tvLaundry = findViewById(R.id.tvLaundry);
        tvWC = findViewById(R.id.tvWC);
        tvShower = findViewById(R.id.tvShower);
        tvNameOfCommand = findViewById(R.id.tvNameOfCommand);
        tvPhoneNumberOfCommand = findViewById(R.id.tvPhoneNumberOfCommand);
        tvMapMarker = findViewById(R.id.tvMapMarker);
        btnInterview = findViewById(R.id.btnInterview);
        btnAllFacilities = findViewById(R.id.btnAllFacilities);


        tvCountOfImages.setText(String.valueOf(listOfImage.size()));
        tvHostelTitle.setText(hostel.getTitle());
        tvHostelAddress.setText(hostel.getAddress());
        tvPrice.setText(hostel.getPrice());
        tvTypeOfHostels.setText(hostel.getType());
        tvCountOfNeighbors.setText(hostel.getRoominess().replaceAll("^\\D\\s\\S+\\s",""));
        tvLaundry.setText(hostel.getLaundry());
        tvWC.setText(hostel.getToilet().replaceAll("^\\S+\\s","").substring(0,1).toUpperCase()+hostel.getToilet().replaceAll("^\\S+\\s","").substring(1));
        tvShower.setText(hostel.getShower().replaceAll("^\\S+\\s","").substring(0,1).toUpperCase()+hostel.getShower().replaceAll("^\\S+\\s","").substring(1));
        tvNameOfCommand.setText(hostel.getCommandant());
        tvPhoneNumberOfCommand.setText(hostel.getCommandantPhone());

        latitude = hostel.getLatitude();
        longitude = hostel.getLongitude();

        tvMapMarker.setOnClickListener(v -> {

            fragment = new MapEntrantFragment();

            Bundle bundle = new Bundle();
            bundle.putDouble("latitude", latitude);
            bundle.putDouble("longitude", longitude);
            bundle.putString("title", hostel.getTitle());
            bundle.putString("address", hostel.getAddress());
            fragment.setArguments(bundle);

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.constraintLayoutHostel, fragment, "tag");
            ft.addToBackStack(null);
            ft.commit();
        });

        btnAllFacilities.setOnClickListener(v -> {

            fragment = new HostelsFacilitiesFragment();

            Bundle bundle = new Bundle();
            bundle.putInt("position", position);
            fragment.setArguments(bundle);

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.constraintLayoutHostel, fragment, "tag");
            ft.addToBackStack(null);
            ft.commit();
        });

        btnInterview.setOnClickListener(v -> {

            fragment = new InterviewWithResidentOfHostelFragment();

            String interview = hostel.getInterview();
            Bundle bundle = new Bundle();
            bundle.putString("interview", interview);
            fragment.setArguments(bundle);

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.constraintLayoutHostel, fragment, "tag");
            ft.addToBackStack(null);
            ft.commit();
        });

        setSupportActionBar(findViewById(R.id.toolbar));
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapsingToolbarLayout = findViewById(R.id.collapsToolBar);
        collapsingToolbarLayout.setTitle(hostel.getTitle());
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        carouselView = findViewById(R.id.carousel);
        carouselView.setPageCount(listOfImage.size());
        carouselView.setImageListener(imageListener);
        carouselView.addOnPageChangeListener(pageChangeListener);

        carouselView.setImageClickListener(position1 -> {
            Intent intent1 = new Intent(HostelDescriptionActivity.this, HostelCarouselViewActivity.class);
            intent1.putExtra("position", position1);
            //TODO
            intent1.putExtra("images", images);
            startActivity(intent1);
        });
    }

    private ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            Picasso.get().load(listOfImage.get(position).getImage()).into(imageView);

        }
    };

    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            tvCurrentNumber.setText(String.valueOf(position + 1));
        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
