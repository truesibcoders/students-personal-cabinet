package com.truecoders.cabinet.Utils;

import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * @author Maria Fabrichkina
 */

public class ZoomAnimation implements ViewPager.PageTransformer {

    @Override
    public void transformPage(@NonNull View view, float position) {
        int pageWidth = view.getWidth();
        int pageHeight = view.getHeight();

        if (position < -1) {
            view.setAlpha(0f);
        } else if (position <= 1) {
            float MIN_SCALE = 0.75f;
            float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
            float vMargin = pageHeight * (1 - scaleFactor) / 2;
            float hMargin = pageWidth * (1 - scaleFactor) / 2;
            if (position<0){
                view.setTranslationX(hMargin-vMargin/2);
            }else {
                view.setTranslationX(-hMargin+vMargin/2);
            }

            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);

            float MIN_ALPHA = 0.5f;
            view.setAlpha(MIN_ALPHA +(scaleFactor- MIN_SCALE)/(1- MIN_SCALE)*(1- MIN_ALPHA));
        }
        else {
            view.setAlpha(0f);
        }
    }
}
