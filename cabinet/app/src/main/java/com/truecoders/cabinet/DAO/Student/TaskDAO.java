package com.truecoders.cabinet.DAO.Student;

import com.truecoders.cabinet.Model.Student.Task;
import io.realm.Realm;

import java.util.UUID;

/**
 * @author Andrew Telkov
 */

public class TaskDAO {

    public static void addTaskToRealm(Task task) {
//        Profile profile = Realm.getDefaultInstance().where(Profile.class).findFirst();

        Realm.getDefaultInstance().executeTransaction(realm -> {
            Task realmObject = realm.createObject(Task.class, UUID.randomUUID().getMostSignificantBits());
            realmObject.setTitle(task.getTitle());
            realmObject.setDescription(task.getDescription());
            realmObject.setDate(task.getDate());
//            realmObject.setIdOwner(Objects.requireNonNull(profile).getIdStudentCard());
        });
    }

    public static void updateTaskIntoRealm(Task task) {
        Realm.getDefaultInstance().executeTransaction(realm -> {
            Task realmObject = realm.where(Task.class).equalTo("id", task.getId()).findFirst();
            if (realmObject != null) {
                realmObject.setTitle(task.getTitle());
                realmObject.setDescription(task.getDescription());
                realmObject.setDate(task.getDate());
            }
        });
    }
}
