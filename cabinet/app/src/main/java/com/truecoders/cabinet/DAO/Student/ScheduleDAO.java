package com.truecoders.cabinet.DAO.Student;

import com.truecoders.cabinet.Model.Student.Schedule;
import io.realm.Realm;

/**
 * @author Ilya Baykalov
 */

public class ScheduleDAO {

//    public static List<Schedule> getScheduleData(String response, Callback callback) throws JSONException {
//        List<Schedule> scheduleList = new ArrayList<>();
//        Integer currentWeek = new JSONObject(response).getInt("week");
//        JSONArray jsonArray = new JSONObject(response).getJSONArray("data");
//
//        for (int i = 0; i < jsonArray.length(); i++) {
//            JSONObject tempObject = jsonArray.getJSONObject(i);
//            String subjectName = tempObject.getString("subjectName");
//            Integer week = tempObject.getInt("week");
//            String classroom = tempObject.getString("room");
//            String teacher = tempObject.getString("teacher");
//            Integer day = tempObject.getInt("day");
//            Integer time = tempObject.getInt("time");
//            Boolean subjectType = tempObject.getBoolean("subjectType");
//
//            scheduleList.add(new Schedule(subjectName, week, classroom, teacher, day, time, subjectType));
//        }
//
//        callback.onSuccess(currentWeek);
//        return scheduleList;
//    }

    public static void addScheduleToRealm(Schedule schedule) {
        Realm.getDefaultInstance().executeTransaction(realm -> {
            Schedule realmObject = realm.createObject(Schedule.class);
            realmObject.setSubjectName(schedule.getSubjectName());
            realmObject.setRoom(schedule.getRoom());
            realmObject.setDay(schedule.getDay());
            realmObject.setSubjectType(schedule.getSubjectType());
            realmObject.setTeacher(schedule.getTeacher());
            realmObject.setTime(schedule.getTime());
            realmObject.setWeek(schedule.getWeek());
        });
    }
}
