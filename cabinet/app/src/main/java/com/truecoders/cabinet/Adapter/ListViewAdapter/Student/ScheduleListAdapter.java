package com.truecoders.cabinet.Adapter.ListViewAdapter.Student;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.truecoders.cabinet.Model.Student.Schedule;
import com.truecoders.cabinet.R;

import java.util.List;

/**
 * @author Ilya Baykalov
 */

public class ScheduleListAdapter extends ArrayAdapter<Schedule> {
    private LayoutInflater inflater;
    private List<Schedule> scheduleList;

    public ScheduleListAdapter(Context context, List<Schedule> scheduleList) {
        super(context, R.layout.adapter_schedule, scheduleList);
        this.scheduleList = scheduleList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.adapter_schedule, parent, false);
        }

        Schedule schedule = scheduleList.get(position);

        ((TextView) view.findViewById(R.id.schedule_title)).setText(String.format("%s", schedule.getSubjectName()));

        if (schedule.getTeacher().isEmpty()) {
            view.findViewById(R.id.schedule_teacher).setVisibility(View.GONE);
        } else {
            ((TextView) view.findViewById(R.id.schedule_teacher)).setText(String.format("%s", schedule.getTeacher()));
        }

        if (schedule.getRoom().isEmpty()) {
            view.findViewById(R.id.schedule_classroom).setVisibility(View.GONE);
        } else {
            ((TextView) view.findViewById(R.id.schedule_classroom)).setText(String.format("%s", schedule.getRoom()));
        }

        if (schedule.getTime() != null) {
            switch (schedule.getTime()) {
                case 1:
                    ((TextView) view.findViewById(R.id.schedule_datetime)).setText(String.format("%s", "8:30–10:05"));
                    break;
                case 2:
                    ((TextView) view.findViewById(R.id.schedule_datetime)).setText(String.format("%s", "10:15–11:50"));
                    break;
                case 3:
                    ((TextView) view.findViewById(R.id.schedule_datetime)).setText(String.format("%s", "12:00–13:35"));
                    break;
                case 4:
                    ((TextView) view.findViewById(R.id.schedule_datetime)).setText(String.format("%s", "14:10–15:45"));
                    break;
                case 5:
                    ((TextView) view.findViewById(R.id.schedule_datetime)).setText(String.format("%s", "15:55–17:30"));
                    break;
                case 6:
                    ((TextView) view.findViewById(R.id.schedule_datetime)).setText(String.format("%s", "17:40–19:15"));
                    break;
                case 7:
                    ((TextView) view.findViewById(R.id.schedule_datetime)).setText(String.format("%s", "19:25–21:00"));
                    break;
            }
        }

        if (schedule.getSubjectType()!=null) {
            ((TextView) view.findViewById(R.id.schedule_subject_type)).setText(String.format("%s", schedule.getSubjectType()));

            if (schedule.getSubjectType()) {
                ((TextView) view.findViewById(R.id.schedule_subject_type)).setText(String.format("%s", "Лекция"));
                view.findViewById(R.id.subject_type_layout).setBackgroundResource(R.drawable.adapter_schedule_background_subject_type_lecture);
            } else {
                ((TextView) view.findViewById(R.id.schedule_subject_type)).setText(String.format("%s", "Практика"));
                view.findViewById(R.id.subject_type_layout).setBackgroundResource(R.drawable.adapter_schedule_background_subject_type_practice);
            }
        }

        return view;
    }
}