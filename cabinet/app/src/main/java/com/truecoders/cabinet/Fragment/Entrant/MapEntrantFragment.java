package com.truecoders.cabinet.Fragment.Entrant;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.truecoders.cabinet.Model.Entrant.Marker;
import com.truecoders.cabinet.Model.General.BaseClass;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.RetroClient;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

import static com.truecoders.cabinet.DAO.Entrant.MarkerDAO.addMarkerToRealm;

/**
 * @author Maria Fabrichkina
 */

public class MapEntrantFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraMoveListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener {

    private static final String TAG = "MyApp";
    private GoogleMap mMap;
    private float zoom;
    private int flag = 1, tag = 0;
    private double latitude, longitude;
    private String titleOfHostel, addressOfHostel;

    private static final int PERMISSIONS_REQUESTS = 2;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map_university_entrant, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_map);

        mapFragment.getMapAsync(this);

        Button buttonCircle = view.findViewById(R.id.buttonCircle);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tag = 1;
            latitude = bundle.getDouble("latitude");
            longitude = bundle.getDouble("longitude");
            titleOfHostel = bundle.getString("title");
            addressOfHostel = bundle.getString("address");

            buttonCircle.setVisibility(View.GONE);

        } else {

            buttonCircle.setOnClickListener(view1 -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                builder.setTitle("Обозначения")
                        .setIcon(R.drawable.ic_attention_mark_24)
                        .setView(R.layout.view_map_alert_info);

                builder.setNegativeButton("Ок",
                        (dialog, id) -> dialog.cancel());

                AlertDialog alert = builder.create();
                alert.show();
            });
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            requestMultiplePermissions();

        } else {
            mMap.setMyLocationEnabled(true);
        }

        if (tag == 1) {

            LatLng locationOnHostel = new LatLng(latitude, longitude);
            mMap.addMarker(new MarkerOptions().position(locationOnHostel).title(titleOfHostel).
                    snippet(addressOfHostel)).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placemark_home));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationOnHostel, 16F));
        } else {

            mMap.setOnCameraMoveStartedListener(this);
            mMap.setOnCameraMoveListener(this);

            addMarkers();
            addBigMarkers();

            LatLng locationCentre = new LatLng(56.011753, 92.8292229);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationCentre, 11F));
            zoom = Float.parseFloat("11");
        }

    }

    private void addMarkers() {
        if (Realm.getDefaultInstance().where(Marker.class).count() == 0) {
            RetroClient.getApiService().getMarkers()
                    .enqueue(new Callback<BaseClass>() {
                        @Override
                        public void onResponse(@NonNull Call<BaseClass> call, @NonNull Response<BaseClass> response) {
                            List<Marker> markerList = response.body() != null ? response.body().getMarkers() : null;

                            if (markerList != null) {
                                for (Marker marker : markerList) {
                                    addMarkerToRealm(marker);
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<BaseClass> call, @NonNull Throwable t) {

                        }
                    });
        } else {
        }
    }

    private void addBigMarkers() {

        mMap.clear();
        flag = 1;

        double[] length = {55.98479, 55.99542, 56.0007, 56.019547, 56.0139152, 56.0053272};
        double[] breadth = {92.75395, 92.79239, 92.93592, 92.843143, 92.8716305, 92.768848};

        for (int i = 0; i < length.length; i++) {
            LatLng location = new LatLng(length[i], breadth[i]);
            mMap.addMarker(new MarkerOptions().position(location).title("Площадки").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cluster_sibfu)));
        }
        mMap.setOnMarkerClickListener(this);
    }

    private void addSmallMarkers() {

        mMap.clear();
        flag = 0;

        List<Marker> MarkerList = Realm.getDefaultInstance().where(Marker.class).findAll();

        for (int i = 0; i < MarkerList.size(); i++) {

            int iconMarkers = MarkerList.get(i).getIcon();

            LatLng position = new LatLng(MarkerList.get(i).getLatitude(), MarkerList.get(i).getLongitude());

            switch (iconMarkers) {
                case 0:
                    mMap.addMarker(new MarkerOptions().position(position).title(MarkerList.get(i).getTitle()).snippet(MarkerList.get(i).getSnippet()).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placemark_university)));
                    break;
                case 1:
                    mMap.addMarker(new MarkerOptions().position(position).title(MarkerList.get(i).getTitle()).snippet(MarkerList.get(i).getSnippet()).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placemark_apartment)));
                    break;
                case 2:
                    mMap.addMarker(new MarkerOptions().position(position).title(MarkerList.get(i).getTitle()).snippet(MarkerList.get(i).getSnippet()).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placemark_home)));
                    break;
                case 3:
                    mMap.addMarker(new MarkerOptions().position(position).title(MarkerList.get(i).getTitle()).snippet(MarkerList.get(i).getSnippet()).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placemark_sport)));
                    break;
                case 4:
                    mMap.addMarker(new MarkerOptions().position(position).title(MarkerList.get(i).getTitle()).snippet(MarkerList.get(i).getSnippet()).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placemark_health)));
                    break;
                default:
                    mMap.addMarker(new MarkerOptions().position(position).title(MarkerList.get(i).getTitle()).snippet(MarkerList.get(i).getSnippet()));
                    break;
            }
        }
    }

    @Override
    public void onCameraMoveStarted(int reason) {

        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            Log.i(TAG, "The user gestured on the map.");
            float currentZoom = mMap.getCameraPosition().zoom;
            Log.i(TAG, String.format("%s", currentZoom));
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_API_ANIMATION) {
            Log.i(TAG, "The user tapped something on the map.");
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_DEVELOPER_ANIMATION) {
            Log.i(TAG, "The app moved the camera");
        }
    }

    @Override
    public void onCameraMove() {
        float currentZoom = mMap.getCameraPosition().zoom;

        if (currentZoom != zoom) {
            if ((currentZoom > 13) && (flag == 1)) {
                addSmallMarkers();
                zoom = currentZoom;
            } else if ((currentZoom < 13) && (flag == 0)) {
                addBigMarkers();
                zoom = currentZoom;
            } else {
                zoom = currentZoom;
            }
        }
    }

    @Override
    public boolean onMarkerClick(com.google.android.gms.maps.model.Marker marker) {

        if (marker.getTitle().equals("Площадки")) {
            addSmallMarkers();
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 15F));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(getContext(), "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(getContext(), "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    public void requestMultiplePermissions() {
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                PERMISSIONS_REQUESTS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
