package com.truecoders.cabinet.Fragment.Student;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.truecoders.cabinet.Adapter.ListViewAdapter.Student.CareerListAdapter;
import com.truecoders.cabinet.Model.General.BaseClass;
import com.truecoders.cabinet.Model.Student.Arragment;
import com.truecoders.cabinet.Model.Student.Profile;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.RetroClient;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CareerFragment extends Fragment implements View.OnClickListener {

    private Button btnCurrent;
    private Button btnPassed;
    private ListView eventsListView;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_career, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        eventsListView = view.findViewById(R.id.list_events);
        progressBar = view.findViewById(R.id.spin_kit3);
        btnCurrent = view.findViewById(R.id.btn_current_events);
        btnPassed = view.findViewById(R.id.btn_passed_events);
        btnPassed.setOnClickListener(this);
        btnCurrent.setOnClickListener(this);

        btnCurrent.setBackgroundResource(R.drawable.background_invisible);
        btnPassed.setBackgroundResource(R.color.background_buttons_document);

        getCareer(1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_current_events:
                btnCurrent.setBackgroundResource(R.drawable.background_invisible);
                btnPassed.setBackgroundResource(R.color.background_buttons_document);
                getCareer(1);
                break;
            case R.id.btn_passed_events:
                btnPassed.setBackgroundResource(R.drawable.background_invisible);
                btnCurrent.setBackgroundResource(R.color.background_buttons_document);
                getCareer(2);
                break;
        }
    }

    private void getCareer(int status) {
        RetroClient.getApiService().getCareer(
                Objects.requireNonNull(Realm.getDefaultInstance().where(Profile.class).findFirst()).getUsername(),
                Objects.requireNonNull(Realm.getDefaultInstance().where(Profile.class).findFirst()).getPassword()
        ).enqueue(new Callback<BaseClass>() {
            @Override
            public void onResponse(@NonNull Call<BaseClass> call, @NonNull Response<BaseClass> response) {
                if (response.body() != null) {
                    List<Arragment> eventList = response.body().getArragment();
                    List<Arragment> currentEventList = new ArrayList<>();
                    List<Arragment> passedEventList = new ArrayList<>();
                    for (int i = 0; i < eventList.size(); i++) {
                        if (eventList.get(i).getStatus().equals("end")) {
                            passedEventList.add(eventList.get(i));
                        } else {
                            currentEventList.add(eventList.get(i));
                        }
                    }

                    if (status == 1) {
                        CareerListAdapter adapter = new CareerListAdapter(getContext(), currentEventList);
                        eventsListView.setAdapter(adapter);
                        progressBar.setVisibility(View.GONE);
                    } else {
                        CareerListAdapter adapter = new CareerListAdapter(getContext(), passedEventList);
                        eventsListView.setAdapter(adapter);
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseClass> call, @NonNull Throwable t) {

            }
        });
    }
}
