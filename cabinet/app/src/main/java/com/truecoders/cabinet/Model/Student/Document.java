package com.truecoders.cabinet.Model.Student;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Ilya Baykalov
 */

public class Document implements Serializable {

    private Date createDate, doneDate;
    private Integer registerNumber, idTFS, idFS, idFSS;
    private Boolean officialSeal, isDone;

    public Document(Integer idFS, Integer idFSS, Integer registerNumber, Integer idTFS, Boolean officialSeal, Date createDate, Boolean isDone, Date doneDate) {
        this.idFS = idFS;
        this.idFSS = idFSS;
        this.createDate = createDate;
        this.doneDate = doneDate;
        this.registerNumber = registerNumber;
        this.idTFS = idTFS;
        this.officialSeal = officialSeal;
        this.isDone = isDone;
    }

    public Integer getIdFs() {
        return idFS;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public Date getDoneDate() {
        return doneDate;
    }

    public Boolean isDone() {
        return isDone;
    }

    public Integer getIdTFS() {
        return idTFS;
    }

    public Integer getIdFSS() {
        return idFSS;
    }

}