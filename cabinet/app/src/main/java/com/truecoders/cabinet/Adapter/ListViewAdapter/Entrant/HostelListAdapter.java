package com.truecoders.cabinet.Adapter.ListViewAdapter.Entrant;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.truecoders.cabinet.Activity.Entrant.HostelDescriptionActivity;
import com.truecoders.cabinet.Model.Entrant.Hostel;
import com.truecoders.cabinet.R;

import java.util.List;

/**
 * @author Maria Fabrichkina
 */

public class HostelListAdapter extends ArrayAdapter<Hostel> {

    private LayoutInflater inflater;
    private List<Hostel> hostels;

    public HostelListAdapter(@NonNull Context context, List<Hostel> hostels) {
        super(context, R.layout.adapter_hostel, hostels);
        this.hostels = hostels;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.adapter_hostel, parent, false);
        }

        ((TextView) view.findViewById(R.id.hostel_title)).setText(String.format("%s", hostels.get(position).getTitle()));

        if (!hostels.get(position).getTitleImage().equals("null")) {
            Picasso.get()
                    .load(hostels.get(position).getTitleImage())
                    .placeholder(R.drawable.ic_sfu_logo_24dp)
                    .into(((ImageView) view.findViewById(R.id.background_image)));
        } else {
            ((ImageView) view.findViewById(R.id.background_image)).setImageResource(R.drawable.ic_sfu_logo_24dp);
        }

        view.findViewById(R.id.btn_read).setOnClickListener(v -> {

            Intent intent = new Intent(getContext(), HostelDescriptionActivity.class);
            intent.putExtra("position", position);
            getContext().startActivity(intent);
        });
        return view;
    }
}
