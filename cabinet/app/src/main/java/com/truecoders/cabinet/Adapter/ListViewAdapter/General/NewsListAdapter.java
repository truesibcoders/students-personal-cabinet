package com.truecoders.cabinet.Adapter.ListViewAdapter.General;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.truecoders.cabinet.Activity.General.InfoNewsActivity;
import com.truecoders.cabinet.Model.General.News;
import com.truecoders.cabinet.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * @author Ilya Baykalov
 */

public class NewsListAdapter extends ArrayAdapter<News> {
    private LayoutInflater inflater;
    private List<News> newsList;

    public NewsListAdapter(Context context, List<News> newsList) {
        super(context, R.layout.adapter_news, newsList);
        this.newsList = newsList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.adapter_news, parent, false);
        }

        final News news = newsList.get(position);

        ((TextView) view.findViewById(R.id.hostel_title)).setText(String.format("%s", news.getTitle()));
        //((TextView) view.findViewById(R.id.news_category)).setText(String.format("%s", news.getCategory()));

        if (!news.getImageUrl().equals("null")) {
            Picasso.get()
                    .load(news.getImageUrl())
                    .placeholder(R.drawable.ic_sfu_logo_24dp)
                    .into(((ImageView) view.findViewById(R.id.background_image)));
         }
         else {
            ((ImageView) view.findViewById(R.id.background_image)).setImageResource(R.drawable.ic_sfu_logo_24dp);
        }

        view.findViewById(R.id.btn_read).setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), InfoNewsActivity.class);
            intent.putExtra("news", news);
            getContext().startActivity(intent);
        });
        return view;
    }


}