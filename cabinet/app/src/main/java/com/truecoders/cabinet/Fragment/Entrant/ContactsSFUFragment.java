package com.truecoders.cabinet.Fragment.Entrant;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.truecoders.cabinet.Adapter.ListViewAdapter.Entrant.ContactListAdapter;
import com.truecoders.cabinet.Model.Entrant.Contact;
import com.truecoders.cabinet.Model.General.BaseClass;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.RetroClient;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

import static com.truecoders.cabinet.DAO.Entrant.ContactsDAO.addContactToRealm;

/**
 * @author Maria Fabrichkina
 */

public class ContactsSFUFragment extends Fragment {
    private ListView listViewContacts;
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts_sfu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listViewContacts = view.findViewById(R.id.listViewContacts);
        progressBar = view.findViewById(R.id.spin_kit);

        addContact();
    }

    private void addContact() {
        if (Realm.getDefaultInstance().where(Contact.class).count() == 0) {
            RetroClient.getApiService().getContact()
                    .enqueue(new Callback<BaseClass>() {
                        @Override
                        public void onResponse(@NonNull Call<BaseClass> call, @NonNull Response<BaseClass> response) {
                            List<Contact> contactListList = response.body() != null ? response.body().getContacts() : null;

                            if (contactListList != null) {
                                for (Contact contact : contactListList) {
                                    addContactToRealm(contact);
                                }
                            }
                            showContact();
                        }

                        @Override
                        public void onFailure(@NonNull Call<BaseClass> call, @NonNull Throwable t) {

                        }
                    });
        } else {
            showContact();
        }
    }

    private void showContact() {
        List<Contact> contactList = Realm.getDefaultInstance().where(Contact.class).findAll();
        ContactListAdapter adapter = new ContactListAdapter(getContext(), contactList);
        progressBar.setVisibility(View.GONE);
        listViewContacts.setAdapter(adapter);
    }
}
