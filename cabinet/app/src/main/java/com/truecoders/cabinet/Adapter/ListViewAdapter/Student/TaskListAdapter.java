package com.truecoders.cabinet.Adapter.ListViewAdapter.Student;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.truecoders.cabinet.Activity.Student.TaskCreateEditActivity;
import com.truecoders.cabinet.Model.Student.Task;
import com.truecoders.cabinet.R;
import io.realm.Realm;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;

/**
 * @author Ilya Baykalov
 */

public class TaskListAdapter extends ArrayAdapter<Task> {
    private LayoutInflater inflater;
    private List<Task> taskList;
    private ListView listView;

    public TaskListAdapter(Context context, ListView listView, List<Task> taskList) {
        super(context, R.layout.adapter_tasks, taskList);
        this.taskList = taskList;
        this.listView = listView;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    @SuppressLint("SimpleDateFormat")
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.adapter_tasks, parent, false);
        }
        ((TextView) view.findViewById(R.id.task_title)).setText(String.format("%s", taskList.get(position).getTitle()));
        if (!taskList.get(position).getDescription().isEmpty()) {
            ((TextView) view.findViewById(R.id.task_description)).setText(String.format("%s", taskList.get(position).getDescription()));
        } else {
            view.findViewById(R.id.task_description).setVisibility(View.GONE);
        }

        if (!taskList.get(position).getDate().toString().isEmpty()) {
            ((TextView) view.findViewById(R.id.task_datetime)).setText(String.format("%s",
                    new SimpleDateFormat("dd MMMM yyyy").format(taskList.get(position).getDate())));
        } else {
            view.findViewById(R.id.task_calendar).setVisibility(View.GONE);
            view.findViewById(R.id.task_datetime).setVisibility(View.GONE);
        }

        view.findViewById(R.id.btn_delete).setOnClickListener(v -> {
            Realm.getDefaultInstance().executeTransaction(realm ->
                    Objects.requireNonNull(realm.where(Task.class).equalTo("id", taskList.get(position).getId())
                            .findFirst())
                            .deleteFromRealm());

            // TODO: fix listeners!!!
            taskList = Realm.getDefaultInstance().where(Task.class).findAll();
            TaskListAdapter adapter = new TaskListAdapter(getContext(), listView, taskList);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener((parent1, view1, position1, id) -> {
                Task task = new Task();
                task.setId(Objects.requireNonNull(taskList.get(position1)).getId());
                task.setTitle(Objects.requireNonNull(taskList.get(position1)).getTitle());
                task.setDescription(Objects.requireNonNull(taskList.get(position1)).getDescription());
                task.setDate(Objects.requireNonNull(taskList.get(position1)).getDate());

                Intent intent = new Intent(getContext(), TaskCreateEditActivity.class);
                intent.putExtra("editMode", true);
                intent.putExtra("task", task);
                getContext().startActivity(intent);
            });
        });

        return view;
    }
}