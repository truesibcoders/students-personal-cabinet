package com.truecoders.cabinet.DAO.Student;

import com.truecoders.cabinet.Model.Student.Profile;
import io.realm.Realm;

/**
 * @author Ilya Baykalov
 */

public class ProfileDAO {

    public static void addProfileToRealm(Profile profile) {
        Realm.getDefaultInstance().executeTransaction(realm -> {
            Profile realmObject = realm.createObject(Profile.class);
            realmObject.setSurname(profile.getSurname());
            realmObject.setName(profile.getName());
            realmObject.setPatronymic(profile.getPatronymic());
            realmObject.setInstitute(profile.getInstitute());
            realmObject.setGroupName(profile.getGroupName());
            realmObject.setGroupLink(profile.getGroupLink());
            realmObject.setRecordBook(profile.getRecordBook());
            realmObject.setEmail(profile.getEmail());
            realmObject.setUsername(profile.getUsername());
            realmObject.setPassword(profile.getPassword());
            realmObject.setBirthday(profile.getBirthday());
            realmObject.setAvatar(profile.getAvatar());
    });
}
}
