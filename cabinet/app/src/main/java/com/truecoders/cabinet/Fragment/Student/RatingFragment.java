package com.truecoders.cabinet.Fragment.Student;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.truecoders.cabinet.R;

/**
 * @author Ilya Baykalov
 */

public class RatingFragment extends Fragment implements View.OnClickListener {

    private Button btnRecordBook;
    private Button btnProgress;
    private TextView titleActivity;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rating, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null) {
            titleActivity = getActivity().findViewById(R.id.header_title);
        }

        btnRecordBook = view.findViewById(R.id.btn_recordbook);
        btnProgress = view.findViewById(R.id.btn_progress);
        btnRecordBook.setOnClickListener(this);
        btnProgress.setOnClickListener(this);

        displaySelectedScreen(0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_progress:
                displaySelectedScreen(0);
                break;
            case R.id.btn_recordbook:
                displaySelectedScreen(1);
                break;
        }
    }

    private void displaySelectedScreen(int code) {
        Fragment fragment = null;
        switch (code) {
            case 0:
                fragment = new ProgressFragment();
                titleActivity.setText(R.string.progress_button);
                btnProgress.setBackgroundResource(R.drawable.background_invisible);
                btnRecordBook.setBackgroundResource(R.color.background_buttons_document);
                break;
            case 1:
                fragment = new RecordBookFragment();
                titleActivity.setText(R.string.recordbook_button);
                btnRecordBook.setBackgroundResource(R.drawable.background_invisible);
                btnProgress.setBackgroundResource(R.color.background_buttons_document);
                break;
        }
        if (fragment != null) {
            if (getFragmentManager() != null) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.progress_content_frame, fragment);
                ft.commit();
            }
        }
    }
}
