package com.truecoders.cabinet.Activity.General;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.truecoders.cabinet.Activity.Student.StudentActivity;
import com.truecoders.cabinet.Model.Student.Profile;
import com.truecoders.cabinet.R;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * @author Ilya Baykalov
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder()
                .name("cabinet.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfig);

        if (Realm.getDefaultInstance().where(Profile.class).count() == 0) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            startActivity(new Intent(this, StudentActivity.class));
        }
    }
}
