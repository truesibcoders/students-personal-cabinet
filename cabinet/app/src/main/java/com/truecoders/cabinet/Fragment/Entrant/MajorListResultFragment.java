package com.truecoders.cabinet.Fragment.Entrant;

import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.truecoders.cabinet.Adapter.ListViewAdapter.Entrant.MajorListAdapter;
import com.truecoders.cabinet.Model.Entrant.Major;
import com.truecoders.cabinet.Model.Entrant.RequireSubject;
import com.truecoders.cabinet.Model.General.BaseClass;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.RetroClient;
import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

import static com.truecoders.cabinet.DAO.Entrant.MajorDAO.addMajorToRealm;

/**
 * Created by Maria Fabrichkina on 13.03.19
 */

public class MajorListResultFragment extends Fragment {

    private ListView listView;
    private ProgressBar progressBar;
    private Boolean cbInternal, cbCorrespondence, cbInternalCorrespondence, cbBudget, cbPay, selectedMath, selectedRussian,
            selectedPhysic, selectedHistory, selectedBiology, selectedInformatics, selectedChemistry,
            selectedLiterature, selectedInternal, selectedForeignLanguage, selectedSocialScience, selectedGeography;
    int etCost;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_major_list_result, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listView = view.findViewById(R.id.lvMajor);
        progressBar = view.findViewById(R.id.spin_kit);

        Bundle bundle = this.getArguments();

        cbInternal = bundle.getBoolean("cbInternal");
        cbCorrespondence = bundle.getBoolean("cbCorrespondence");
        cbInternalCorrespondence = bundle.getBoolean("cbInternalCorrespondence");
        cbBudget = bundle.getBoolean("cbBudget");
        cbPay = bundle.getBoolean("cbPay");

        if (cbPay) {
            etCost = Integer.valueOf(bundle.getString("etCost"));
        }

        selectedMath = bundle.getBoolean("selectedMath");
        selectedRussian = bundle.getBoolean("selectedRussian");
        selectedPhysic = bundle.getBoolean("selectedPhysic");
        selectedHistory = bundle.getBoolean("selectedBiology");
        selectedBiology = bundle.getBoolean("selectedBiology");
        selectedInformatics = bundle.getBoolean("selectedInformatics");
        selectedChemistry = bundle.getBoolean("selectedChemistry");
        selectedLiterature = bundle.getBoolean("selectedLiterature");
        selectedInternal = bundle.getBoolean("selectedInternal");//пока не юзаю
        selectedForeignLanguage = bundle.getBoolean("selectedForeignLanguage");
        selectedSocialScience = bundle.getBoolean("selectedSocialScience");
        selectedGeography = bundle.getBoolean("selectedGeography");

        addMajor();
    }

    private void addMajor() {
        if (Realm.getDefaultInstance().where(Major.class).count() == 0) {
            RetroClient.getApiService().getMajors()
                    .enqueue(new Callback<BaseClass>() {
                        @Override
                        public void onResponse(@NonNull Call<BaseClass> call, @NonNull Response<BaseClass> response) {
                            List<Major> majorList = response.body() != null ? response.body().getMajors() : null;

                            if (majorList != null) {
                                for (Major major : majorList) {
                                    addMajorToRealm(major);
                                }
                            }
                            showMajor();
                        }

                        @Override
                        public void onFailure(@NonNull Call<BaseClass> call, @NonNull Throwable t) {

                        }
                    });
        } else {
            showMajor();
        }
    }

    private void showMajor() {

        List<Major> majorList;
        majorList = MajorFiltering();

        if (majorList.isEmpty()) {
            progressBar.setVisibility(View.GONE);
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogMajorTheme);
            builder.setTitle("Ничего не найдено!")
                    .setMessage("Попробуйте снова")
                    .setCancelable(false)
                    .setNegativeButton("Вернуться к фильтрам",
                            (dialog, id) -> {
                                dialog.cancel();

                                FragmentManager fm = getActivity().getSupportFragmentManager();
                                fm.popBackStack();
                            });
            AlertDialog alert = builder.create();
            alert.show();
            alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        } else {
            MajorListAdapter adapter = new MajorListAdapter(getContext(), majorList);
            progressBar.setVisibility(View.GONE);
            listView.setAdapter(adapter);
        }
    }

    public List<Major> MajorFiltering() {

        List<Major> allMajor;
        allMajor = Realm.getDefaultInstance().where(Major.class).findAll();

        List<Major> selectedMajor = new ArrayList<>();
        selectedMajor.clear();

        List<String> majorForm = new ArrayList<>();
        List<String> subjectsList = new ArrayList<>();

        for (Major major : allMajor) {

            majorForm.clear();
            subjectsList.clear();

            Boolean check = true;

            if (cbInternal) {
                majorForm.add("Очно");
            }
            if (cbCorrespondence) {
                majorForm.add("Заочно");
            }
            if (cbInternalCorrespondence) {
                majorForm.add("Очно - заочно");
            }

            if (!majorForm.isEmpty()) {
                if (!majorForm.contains(major.getMajorForm())) {
                    check = false;
                }
            }

            if (cbBudget) {

                if (major.getBudgetaryPlace() == 0) {
                    check = false;
                }
            }

            if (cbPay) {
                if ((major.getPaidPlaces() == 0) || (major.getCost() > etCost)) {
                    check = false;
                }
            }

            if (selectedMath) {
                subjectsList.add("Математика");
            }
            if (selectedRussian) {
                subjectsList.add("Русский язык");
            }
            if (selectedPhysic) {
                subjectsList.add("Физика");
            }
            if (selectedHistory) {
                subjectsList.add("История");
            }
            if (selectedBiology) {
                subjectsList.add("Биология");
            }
            if (selectedInformatics) {
                subjectsList.add("Информатика и ИКТ");
            }
            if (selectedChemistry) {
                subjectsList.add("Химия");
            }
            if (selectedLiterature) {
                subjectsList.add("Литература");
            }
            if (selectedForeignLanguage) {
                subjectsList.add("Иностранный язык");
            }
            if (selectedSocialScience) {
                subjectsList.add("Обществознание");
            }
            if (selectedGeography) {
                subjectsList.add("География");
            }

            if (!subjectsList.isEmpty()) {
                for (String subject : subjectsList) {

                    RealmList<RequireSubject> majorSubjects = major.getSubjects();
                    List<String> subList = new ArrayList<>();

                    for (RequireSubject sub : majorSubjects) {
                        subList.add(sub.getSubjectName());
                    }
                    if (!subList.contains(subject)) {
                        check = false;
                        break;
                    }
                }
            }

            if (selectedInternal) {
                if (major.getInternal().equals("0")) {
                    check = false;
                }
            }

            if (check) {
                selectedMajor.add(major);
            }
        }
        return selectedMajor;
    }
}
