package com.truecoders.cabinet.Model.Student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.realm.RealmList;
import io.realm.RealmObject;

public class Semester extends RealmObject {
    @SerializedName("subjectList")
    @Expose
    private RealmList<Subject> subjectList = null;
    @SerializedName("semesterName")
    @Expose
    private String semesterName;
    @SerializedName("period")
    @Expose
    private String period;

    public RealmList<Subject> getSubjectList() {
        return subjectList;
    }

    public String getSemesterName() {
        return semesterName;
    }

    public void setSemesterName(String semesterName) {
        this.semesterName = semesterName;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
