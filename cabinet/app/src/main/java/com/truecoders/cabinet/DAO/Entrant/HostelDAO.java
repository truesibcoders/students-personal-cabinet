package com.truecoders.cabinet.DAO.Entrant;

import com.truecoders.cabinet.Model.Entrant.Hostel;
import io.realm.Realm;

/**
 * @author Maria Fabrichkina
 */

public class HostelDAO {


    public static void addHostelToRealm(Hostel hostel) {

        Realm.getDefaultInstance().executeTransaction(realm -> {
            Hostel realmObject = realm.createObject(Hostel.class);

            realmObject.setTitle(hostel.getTitle());
            realmObject.setAddress(hostel.getAddress());
            realmObject.setLatitude(hostel.getLatitude());
            realmObject.setLongitude(hostel.getLongitude());
            realmObject.setTitleImage(hostel.getTitleImage());
            realmObject.setPrice(hostel.getPrice());
            realmObject.setCommandant(hostel.getCommandant());
            realmObject.setCommandantPhone(hostel.getCommandantPhone());
            realmObject.setType(hostel.getType());
            realmObject.setRoominess(hostel.getRoominess());
            realmObject.setLaundry(hostel.getLaundry());
            realmObject.setShower(hostel.getShower());
            realmObject.setToilet(hostel.getToilet());
            realmObject.setKitchen(hostel.getKitchen());

            realmObject.setBed(hostel.getBed());
            realmObject.setTable(hostel.getTable());
            realmObject.setChair(hostel.getChair());
            realmObject.setNightstand(hostel.getNightstand());
            realmObject.setWardrobe(hostel.getWardrobe());
            realmObject.setBeddingSet(hostel.getBeddingSet());
            realmObject.setConditionOfFurniture(hostel.getConditionOfFurniture());

            realmObject.setStorage(hostel.getStorage());
            realmObject.setElevator(hostel.getElevator());
            realmObject.setPasses(hostel.getPasses());
            realmObject.setAdvantages(hostel.getAdvantages());
            realmObject.setInterview(hostel.getInterview());
            realmObject.getImages().addAll(hostel.getImages());
        });
    }
}
