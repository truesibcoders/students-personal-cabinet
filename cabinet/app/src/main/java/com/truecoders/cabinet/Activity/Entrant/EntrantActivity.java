package com.truecoders.cabinet.Activity.Entrant;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.truecoders.cabinet.Activity.General.MainActivity;
import com.truecoders.cabinet.Fragment.Entrant.EntrantHandbookFragment;
import com.truecoders.cabinet.Fragment.General.NewsFragment;
import com.truecoders.cabinet.Fragment.Entrant.MapEntrantFragment;
import com.truecoders.cabinet.R;

/**
 * @author Ilya Baykalov
 */

public class EntrantActivity extends AppCompatActivity {

    private TextView titleActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_university_entrant);
        getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorHeader));

        titleActivity = findViewById(R.id.header_title);
        (findViewById(R.id.btn_entrance)).setOnClickListener(studentEntrance);
        BottomNavigationViewEx navigation = findViewById(R.id.activity_university_entrance_navigation);
        navigation.setSelectedItemId(R.id.navigation_info);

        navigation.enableShiftingMode(false);
        navigation.enableAnimation(true);
        navigation.enableItemShiftingMode(false);
        navigation.setTextVisibility(false);
        navigation.setIconSize(30, 30);

        displaySelectedScreen(1);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private View.OnClickListener studentEntrance = v -> {
        Intent intent = new Intent(EntrantActivity.this, MainActivity.class);
        intent.putExtra("isStudent", true);
        startActivity(intent);
    };

    private void displaySelectedScreen(int code) {
        Fragment fragment = null;

        switch (code) {
            case 0:
                fragment = new NewsFragment();
                break;
            case 1:
                fragment = new EntrantHandbookFragment();
                break;
            case 2:
                fragment = new MapEntrantFragment();
                break;
        }
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if (!item.isChecked()) {
                switch (item.getItemId()) {
                    case R.id.navigation_news:
                        titleActivity.setText(R.string.title_news_university_entrant_activity);
                        displaySelectedScreen(0);
                        return true;
                    case R.id.navigation_info:
                        titleActivity.setText(R.string.title_info_university_entrant_activity);
                        displaySelectedScreen(1);
                        return true;
                    case R.id.navigation_map:
                        titleActivity.setText(R.string.title_map_university_entrant_activity);
                        displaySelectedScreen(2);
                        return true;
                }
            }
            return false;
        }
    };
}
