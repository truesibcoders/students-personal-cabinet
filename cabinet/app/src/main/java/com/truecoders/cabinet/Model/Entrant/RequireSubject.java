package com.truecoders.cabinet.Model.Entrant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;

/**
 * @author Ilya Baykalov
 */

public class RequireSubject extends RealmObject {

    @SerializedName("subjectName")
    @Expose
    private String subjectName;
    @SerializedName("points")
    @Expose
    private String points;

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

}