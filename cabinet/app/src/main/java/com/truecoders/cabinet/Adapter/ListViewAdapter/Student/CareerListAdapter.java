package com.truecoders.cabinet.Adapter.ListViewAdapter.Student;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.truecoders.cabinet.Model.Student.Arragment;
import com.truecoders.cabinet.R;

import java.util.List;

public class CareerListAdapter extends ArrayAdapter<Arragment> {
    private LayoutInflater inflater;
    private List<Arragment> eventsList;

    public CareerListAdapter(Context context, List<Arragment> eventsList) {
        super(context, R.layout.adapter_career, eventsList);
        this.eventsList = eventsList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.adapter_career, parent, false);
        }

        final Arragment events = eventsList.get(position);

        ((TextView) view.findViewById(R.id.text_career_title)).setText(String.format("%s", events.getTitle()));

        if (!events.getImage().equals("null")) {
            Picasso.get()
                    .load(events.getImage())
                    .placeholder(R.drawable.ic_sfu_logo_24dp)
                    .into(((ImageView) view.findViewById(R.id.image_career)));
        } else {
            ((ImageView) view.findViewById(R.id.image_career)).setImageResource(R.drawable.ic_sfu_logo_24dp);
        }

        ((TextView) view.findViewById(R.id.text_date_career)).setText(String.format("%s", events.getDate()));
        if ((String.format("%s", events.getPeople())).equals("null")) {
            view.findViewById(R.id.text_people_career).setVisibility(View.GONE);
        } else {
            ((TextView) view.findViewById(R.id.text_people_career)).setText(String.format("%s", events.getPeople()));
        }

        if (events.getStatus().equals("end")) {
            view.findViewById(R.id.btn_send).setVisibility(View.GONE);
        }

        return view;
    }
}
