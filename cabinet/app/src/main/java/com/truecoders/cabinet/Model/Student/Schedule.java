package com.truecoders.cabinet.Model.Student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;

/**
 * @author Ilya Baykalov
 */

public class Schedule extends RealmObject {
    @SerializedName("subjectName")
    @Expose
    private String subjectName;
    @SerializedName("week")
    @Expose
    private Integer week;
    @SerializedName("day")
    @Expose
    private Integer day;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("room")
    @Expose
    private String room;
    @SerializedName("teacher")
    @Expose
    private String teacher;
    @SerializedName("subjectType")
    @Expose
    private Boolean subjectType;

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public Boolean getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(Boolean subjectType) {
        this.subjectType = subjectType;
    }

}
//    private String subjectName, classroom, teacher;
//    private Integer week, day, time;
//    private Boolean subjectType;
//
//    public Schedule() { }
//
//    public Schedule(String subjectName, Integer week, String classroom, String teacher, Integer day, Integer time, Boolean subjectType) {
//        this.subjectName = subjectName;
//        this.week = week;
//        this.classroom = classroom;
//        this.teacher = teacher;
//        this.day = day;
//        this.time = time;
//        this.subjectType = subjectType;
//    }
//
//    public String getSubjectName() {
//        return subjectName;
//    }
//
//    public void setSubjectName(String subjectName) {
//        this.subjectName = subjectName;
//    }
//
//    public String getClassroom() {
//        return classroom;
//    }
//
//    public void setClassroom(String classroom) {
//        this.classroom = classroom;
//    }
//
//    public String getTeacher() {
//        return teacher;
//    }
//
//    public void setTeacher(String teacher) {
//        this.teacher = teacher;
//    }
//
//    public Integer getWeek() {
//        return week;
//    }
//
//    public void setWeek(Integer week) {
//        this.week = week;
//    }
//
//    public Integer getDay() {
//        return day;
//    }
//
//    public void setDay(Integer day) {
//        this.day = day;
//    }
//
//    public Integer getTime() {
//        return time;
//    }
//
//    public void setTime(Integer time) {
//        this.time = time;
//    }
//
//    public Boolean getSubjectType() {
//        return subjectType;
//    }
//
//    public void setSubjectType(Boolean subjectType) {
//        this.subjectType = subjectType;
//    }
//}