package com.truecoders.cabinet.Activity.Entrant;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import com.truecoders.cabinet.Fragment.Entrant.ContactsSFUFragment;
import com.truecoders.cabinet.Fragment.Entrant.HostelsFragment;
import com.truecoders.cabinet.Fragment.Entrant.InstitutesFragment;
import com.truecoders.cabinet.Fragment.Entrant.MajorFilterFormOfStudyFragment;
import com.truecoders.cabinet.R;

/**
 * @author Andrew Telkov
 */

public class EntrantHandbookActivity extends AppCompatActivity {

    @Override
    @SuppressLint("NewApi")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_university_entrant);

        getWindow().setStatusBarColor(getResources().getColor(R.color.colorHeader, getTheme()));

        int code = getIntent().getIntExtra("code", 0);

        displaySelectedScreen(code);
    }

    private void displaySelectedScreen(int code) {
        Fragment fragment = null;
        TextView tvTitle = findViewById(R.id.header_title);
        switch (code) {
            case R.id.btn_directions:
                fragment = new MajorFilterFormOfStudyFragment();
                tvTitle.setText(R.string.title_directions_university_entrant_activity);
                break;
            case R.id.btn_institutes:
                fragment = new InstitutesFragment();
                tvTitle.setText(R.string.title_institute_university_entrant_activity);
                break;
            case R.id.btn_dormitories:
                fragment = new HostelsFragment();
                tvTitle.setText(R.string.title_hostel_university_entrant_activity);
                break;
            case R.id.btn_contacts:
                fragment = new ContactsSFUFragment();
                tvTitle.setText(R.string.title_contacts_university_entrant_activity);
                break;
        }

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.content_frame_inst, fragment);
            ft.commit();
        }
    }
}