package com.truecoders.cabinet.Fragment.Student;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.truecoders.cabinet.Adapter.ListViewAdapter.Student.DocumentsListAdapter;
import com.truecoders.cabinet.Model.Student.Document;
import com.truecoders.cabinet.Model.Student.Profile;
import com.truecoders.cabinet.R;
import io.realm.Realm;

import java.util.List;

import static com.truecoders.cabinet.DAO.Student.DocumentDAO.getOrderHistory;

/**
 * @author Ilya Baykalov
 */

public class OrderHistoryFragment extends Fragment {
    private ListView documentListView;
    private ProgressBar progressBar;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_get_document, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        documentListView = view.findViewById(R.id.list_view_documents);
        progressBar = view.findViewById(R.id.spin_kit);

        Profile profile = Realm.getDefaultInstance().where(Profile.class).findFirst();
        if (profile != null) {
//            new RetroClient().POST("order/get",
//                    "{\"login\":"+ profile.getLogin() +"}",
//                    new Callback() {
//                        @Override
//                        public void onFailure(@NonNull Call call, @NonNull IOException e) {
//                        }
//
//                        @Override
//                        public void onResponse(@NonNull Call call, @NonNull Response response) {
//                            try {
//                                if (response.body() != null) {
//                                    new OrderTask().execute(response.body().string());
//                                }
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
        }
    }

    //TODO: fix leak
    @SuppressLint("StaticFieldLeak")
    private class OrderTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... strings) {
            return strings[0];
        }

        protected void onPostExecute(String result) {
            List<Document> documentList = getOrderHistory(result);

            DocumentsListAdapter adapter = new DocumentsListAdapter(getContext(), documentList);
            documentListView.setAdapter(adapter);
            progressBar.setVisibility(View.GONE);

        }
    }
}
