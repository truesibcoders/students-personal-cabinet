package com.truecoders.cabinet.Model.Student;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.realm.RealmList;
import io.realm.RealmObject;

import java.util.List;

public class Subject extends RealmObject {
    @SerializedName("subjectName")
    @Expose
    private String subjectName;
    @SerializedName("mark")
    @Expose
    private String mark;
    @SerializedName("markDate")
    @Expose
    private String markDate;
    @SerializedName("hours")
    @Expose
    private String hours;
    @SerializedName("teacher")
    @Expose
    private RealmList<String> teacher = null;

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getMarkDate() {
        return markDate;
    }

    public void setMarkDate(String markDate) {
        this.markDate = markDate;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public List<String> getTeacher() {
        return teacher;
    }

    public void setTeacher(RealmList<String> teacher) {
        this.teacher = teacher;
    }

}
