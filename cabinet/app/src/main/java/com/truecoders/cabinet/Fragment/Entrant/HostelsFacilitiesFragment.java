package com.truecoders.cabinet.Fragment.Entrant;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.truecoders.cabinet.Model.Entrant.Hostel;
import com.truecoders.cabinet.R;
import io.realm.Realm;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Maria Fabrichkina on 1.03.19
 */

public class HostelsFacilitiesFragment extends Fragment {

    private int position;
    private TextView tvGeneral, tvFurniture, tvAdditionally;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_hostels_facilities, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            position = bundle.getInt("position");
        }

        List<Hostel> hostelList = Realm.getDefaultInstance().where(Hostel.class).findAll();
        final Hostel hostel = hostelList.get(position);

        tvGeneral = view.findViewById(R.id.tvGeneral);
        tvFurniture = view.findViewById(R.id.tvFurniture);
        tvAdditionally = view.findViewById(R.id.tvAdditionally);

        tvGeneral.setText("");
        if (hostel != null) {

            tvGeneral.append(String.format("1. %s\n\n", hostel.getType()));
            tvGeneral.append(String.format("2. %s\n\n", hostel.getRoominess()));
            tvGeneral.append(String.format("3. %s\n\n", hostel.getLaundry()));
            tvGeneral.append(String.format("4. %s\n\n", hostel.getToilet()));
            tvGeneral.append(String.format("5. %s\n\n", hostel.getShower()));
            tvGeneral.append(String.format("6. %s\n\n", hostel.getKitchen()));

            tvFurniture.setText("");
            tvFurniture.append(String.format("1. %s\n\n", hostel.getBed()));
            tvFurniture.append(String.format("2. %s\n\n", hostel.getTable()));
            tvFurniture.append(String.format("3. %s\n\n", hostel.getChair()));
            tvFurniture.append(String.format("4. %s\n\n", hostel.getNightstand()));
            tvFurniture.append(String.format("5. %s\n\n", hostel.getWardrobe()));
            tvFurniture.append(String.format("6. %s\n\n", hostel.getBeddingSet()));

            tvAdditionally.setText("");
            tvAdditionally.append(String.format("1. %s\n\n", hostel.getStorage()));
            tvAdditionally.append(String.format("2. %s\n\n", hostel.getElevator()));
            tvAdditionally.append(String.format("3. %s\n\n", hostel.getPasses()));
            tvAdditionally.append(String.format("4. %s\n\n", hostel.getAdvantages()));
        }
    }
}
