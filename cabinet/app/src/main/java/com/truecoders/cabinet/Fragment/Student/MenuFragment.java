package com.truecoders.cabinet.Fragment.Student;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import com.truecoders.cabinet.Activity.General.MainActivity;
import com.truecoders.cabinet.Model.Entrant.Institute;
import com.truecoders.cabinet.Model.Student.ECoursesList;
import com.truecoders.cabinet.Model.Student.Profile;
import com.truecoders.cabinet.Model.Student.RecordBook;
import com.truecoders.cabinet.Model.Student.Schedule;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.CircleTransform;
import io.realm.Realm;

/**
 * @author Ilya Baykalov
 */

public class MenuFragment extends Fragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu_student, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Profile profile = Realm.getDefaultInstance().where(Profile.class).findFirst();

        ImageView btnAvatar = view.findViewById(R.id.profile_image);

        if (profile != null) {
            ((TextView) (view.findViewById(R.id.full_name))).setText(profile.getFullName());

            if (profile.getAvatar() != null) {
                Picasso.get()
                        .load(profile.getAvatar())
                        .fit().centerCrop()
                        .transform(new CircleTransform())
                        .into(btnAvatar);
            }
        }

        (view.findViewById(R.id.go_to_profile)).setOnClickListener(this);
        (view.findViewById(R.id.btn_my_group)).setOnClickListener(this);
        (view.findViewById(R.id.btn_order_certificates)).setOnClickListener(this);
        (view.findViewById(R.id.btn_career)).setOnClickListener(this);
        (view.findViewById(R.id.btn_developers)).setOnClickListener(this);
        (view.findViewById(R.id.btn_exit)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        displayFragment(v.getId());
    }

    private void displayFragment(int code) {
        String CURRENT_FRAGMENT = "CURRENT FRAGMENT";

        Fragment fragment = null;
        switch (code) {
            case R.id.go_to_profile:
                fragment = new ProfileFragment();
                break;
            case R.id.btn_my_group:
                fragment = new MyGroupFragment();
                break;
            case R.id.btn_career:
                fragment = new CareerFragment();
            case R.id.btn_order_certificates:
                Toast.makeText(getContext(), "Извините, но я ещё в разработке", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_developers:
                Toast.makeText(getContext(), "Извините, но я ещё в разработке", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_exit:
                Realm.getDefaultInstance().executeTransactionAsync(realm -> {
                    Realm.getDefaultInstance().delete(RecordBook.class);
                    Realm.getDefaultInstance().delete(ECoursesList.class);
                    Realm.getDefaultInstance().delete(Institute.class);
                    Realm.getDefaultInstance().delete(Profile.class);
                    Realm.getDefaultInstance().delete(Schedule.class);
                });
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                break;
        }
        if (fragment != null) {
            if (getFragmentManager() != null) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, fragment);
                ft.addToBackStack(CURRENT_FRAGMENT);
                ft.commit();
            }
        }
    }
}
