package com.truecoders.cabinet.Model.Entrant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.truecoders.cabinet.Model.General.Image;
import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * @author Maria Fabrichkina
 */

public class Hostel extends RealmObject {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("title_image")
    @Expose
    private String titleImage;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("commandant")
    @Expose
    private String commandant;
    @SerializedName("commandantPhone")
    @Expose
    private String commandantPhone;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("roominess")
    @Expose
    private String roominess;
    @SerializedName("laundry")
    @Expose
    private String laundry;
    @SerializedName("shower")
    @Expose
    private String shower;
    @SerializedName("toilet")
    @Expose
    private String toilet;
    @SerializedName("kitchen")
    @Expose
    private String kitchen;
    @SerializedName("bed")
    @Expose
    private String bed;
    @SerializedName("table")
    @Expose
    private String table;
    @SerializedName("chair")
    @Expose
    private String chair;
    @SerializedName("nightstand")
    @Expose
    private String nightstand;
    @SerializedName("wardrobe")
    @Expose
    private String wardrobe;
    @SerializedName("condition_of_furniture")
    @Expose
    private String conditionOfFurniture;
    @SerializedName("bedding_set")
    @Expose
    private String beddingSet;
    @SerializedName("storage")
    @Expose
    private String storage;
    @SerializedName("elevator")
    @Expose
    private String elevator;
    @SerializedName("passes")
    @Expose
    private String passes;
    @SerializedName("advantages")
    @Expose
    private String advantages;
    @SerializedName("interview")
    @Expose
    private String interview;
    @SerializedName("images")
    @Expose
    private RealmList<Image> images = new RealmList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getTitleImage() {
        return titleImage;
    }

    public void setTitleImage(String titleImage) {
        this.titleImage = titleImage;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCommandant() {
        return commandant;
    }

    public void setCommandant(String commandant) {
        this.commandant = commandant;
    }

    public String getCommandantPhone() {
        return commandantPhone;
    }

    public void setCommandantPhone(String commandantPhone) {
        this.commandantPhone = commandantPhone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRoominess() {
        return roominess;
    }

    public void setRoominess(String roominess) {
        this.roominess = roominess;
    }

    public String getLaundry() {
        return laundry;
    }

    public void setLaundry(String laundry) {
        this.laundry = laundry;
    }

    public String getShower() {
        return shower;
    }

    public void setShower(String shower) {
        this.shower = shower;
    }

    public String getToilet() {
        return toilet;
    }

    public void setToilet(String toilet) {
        this.toilet = toilet;
    }

    public String getKitchen() {
        return kitchen;
    }

    public void setKitchen(String kitchen) {
        this.kitchen = kitchen;
    }

    public String getBed() {
        return bed;
    }

    public void setBed(String bed) {
        this.bed = bed;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getChair() {
        return chair;
    }

    public void setChair(String chair) {
        this.chair = chair;
    }

    public String getNightstand() {
        return nightstand;
    }

    public void setNightstand(String nightstand) {
        this.nightstand = nightstand;
    }

    public String getWardrobe() {
        return wardrobe;
    }

    public void setWardrobe(String wardrobe) {
        this.wardrobe = wardrobe;
    }

    public String getConditionOfFurniture() {
        return conditionOfFurniture;
    }

    public void setConditionOfFurniture(String conditionOfFurniture) {
        this.conditionOfFurniture = conditionOfFurniture;
    }

    public String getBeddingSet() {
        return beddingSet;
    }

    public void setBeddingSet(String beddingSet) {
        this.beddingSet = beddingSet;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getElevator() {
        return elevator;
    }

    public void setElevator(String elevator) {
        this.elevator = elevator;
    }

    public String getPasses() {
        return passes;
    }

    public void setPasses(String passes) {
        this.passes = passes;
    }

    public String getAdvantages() {
        return advantages;
    }

    public void setAdvantages(String advantages) {
        this.advantages = advantages;
    }

    public String getInterview() {
        return interview;
    }

    public void setInterview(String interview) {
        this.interview = interview;
    }

    public RealmList<Image> getImages() {
        return images;
    }
}