package com.truecoders.cabinet.Model.Entrant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;

import java.io.Serializable;

/**
 * @author Maria Fabrichkina
 */

public class Marker extends RealmObject implements Serializable {

    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("snippet")
    @Expose
    private String snippet;
    @SerializedName("icon")
    @Expose
    private Integer icon;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }

}
//    private int icon;
//    private String title, snippet;
//    private double latitude, longitude;
//
//    public Marker() {
//
//    }
//
//    public Marker(int icon, double latitude, double longitude, String title, String snippet) {
//
//        this.latitude = latitude;
//        this.longitude = longitude;
//        this.title = title;
//        this.snippet = snippet;
//        this.icon = icon;
//    }
//
//    public double getLatitude() {
//        return latitude;
//    }
//
//    public void setLatitude(double latitude) {
//        this.latitude = latitude;
//    }
//
//    public double getLongitude() {
//        return longitude;
//    }
//
//    public void setLongitude(double longitude) {
//        this.longitude = longitude;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public String getSnippet() {
//        return snippet;
//    }
//
//    public void setSnippet(String snippet) {
//        this.snippet = snippet;
//    }
//
//    public int getIcon() {
//        return icon;
//    }
//
//    public void setIcon(int icon) {
//        this.icon = icon;
//    }
//}
