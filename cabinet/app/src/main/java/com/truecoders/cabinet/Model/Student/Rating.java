package com.truecoders.cabinet.Model.Student;

import java.util.Date;

/**
 * @author Ilya Baykalov
 */

public class Rating {

    private Date dateOfPass, consultationDate;
    private Boolean isExam, isPass, isCP, isCW, isPractice;
    private Integer examRating, passRating, cpRating, cwRating, practiceRating, hoursCount;
    private String teacherFullName, subjectName;

    public Rating(Date dateOfPass, Date consultationDate, Boolean isExam, Boolean isPass, Boolean isCP, Boolean isCW, Boolean isPractice, Integer examRating, Integer passRating, Integer cpRating, Integer cwRating, Integer practiceRating, Integer hoursCount, String teacherFullName, String subjectName) {
        this.dateOfPass = dateOfPass;
        this.consultationDate = consultationDate;
        this.isExam = isExam;
        this.isPass = isPass;
        this.isCP = isCP;
        this.isCW = isCW;
        this.isPractice = isPractice;
        this.examRating = examRating;
        this.passRating = passRating;
        this.cpRating = cpRating;
        this.cwRating = cwRating;
        this.practiceRating = practiceRating;
        this.hoursCount = hoursCount;
        this.teacherFullName = teacherFullName;
        this.subjectName = subjectName;
    }

    public Date getDateOfPass() {
        return dateOfPass;
    }

    //TODO: check field
    public Date getConsultationDate() {
        return consultationDate;
    }

    public Boolean isExam() {
        return isExam;
    }

    public Boolean isPass() {
        return isPass;
    }

    public Boolean isCP() {
        return isCP;
    }

    public Boolean isCW() {
        return isCW;
    }

    public Boolean isPractice() {
        return isPractice;
    }

    public Integer getExamRating() {
        return examRating;
    }

    public Integer getPassRating() {
        return passRating;
    }

    public Integer getCpRating() {
        return cpRating;
    }

    public Integer getCwRating() {
        return cwRating;
    }

    public Integer getPracticeRating() {
        return practiceRating;
    }

    public Integer getHoursCount() {
        return hoursCount;
    }

    public String getInitialsTeacherName() {
        if (!getTeacherFullName().equals("")) {
            return getTeacherFullName().split("\\s")[0].concat(" ")
                    .concat(getTeacherFullName().split("\\s")[1].substring(0, 1)).concat(". ")
                    .concat(getTeacherFullName().split("\\s")[2].substring(0, 1)).concat(".");
        } else return "";
    }

    public String getTeacherFullName() {
        return teacherFullName;
    }

    public String getSubjectName() {
        return subjectName;
    }
}
