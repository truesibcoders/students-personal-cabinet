package com.truecoders.cabinet.Adapter.SpinnerAdapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.truecoders.cabinet.R;

import java.util.List;

/**
 * @author Ilya Baykalov
 */

public class SemesterSpinnerAdapter extends BaseAdapter {

    private final List<String> data;
    private final LayoutInflater inflater;

    public SemesterSpinnerAdapter(Context context, List<String> data) {
        this.data = data;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return initView(position, convertView);
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
    }

    @Override
    public int getCount() {
        return data != null ? data.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return initView(position, convertView);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return true;
    }

    @SuppressLint("InflateParams")
    private View initView(int position, View convertView) {
        try {
            SemesterSpinnerAdapter.ViewHolder holder;
            if (convertView == null) {
                holder = new SemesterSpinnerAdapter.ViewHolder();
                convertView = inflater.inflate(R.layout.spinner_cell_layout, null, false);
                holder.title = convertView.findViewById(R.id.spinner_cell_title);
                convertView.setTag(holder);
            } else {
                holder = (SemesterSpinnerAdapter.ViewHolder) convertView.getTag();
            }
            holder.title.setText(data.get(position));
        } catch (IndexOutOfBoundsException ignored) { }
        return convertView;
    }

    class ViewHolder {
        TextView title;
    }
}