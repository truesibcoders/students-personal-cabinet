package com.truecoders.cabinet.DAO.Entrant;

import com.truecoders.cabinet.Model.Entrant.Major;
import io.realm.Realm;
/**
 * Created by Maria Fabrichkina on 15.03.19
 */
public class MajorDAO {

//    public static List<Major> getMajorData(String response) {
//        List<Major> majorList = new ArrayList<>();
//        try {
//            JSONArray jsonArray = new JSONObject(response).getJSONArray("data");
//
//            for (int i = 0; i < jsonArray.length(); i++) {
//
//                JSONObject tempObject = jsonArray.getJSONObject(i);
//                String title = tempObject.getString("title");
//                String university = tempObject.getString("university");
//                String levelOfEducation = tempObject.getString("levelOfEducation");
//                String majorForm = tempObject.getString("majorForm");
//                int budgetaryPlace = tempObject.getInt("budgetaryPlace");
//                int specialPlace = tempObject.getInt("specialPlace");
//                int paidPlaces = tempObject.getInt("paidPlaces");
//                int cost = tempObject.getInt("cost");
//                String averagePoint = tempObject.getString("averagePoint");
//                String entrancePoint = tempObject.getString("entrancePoint");
//                String internal = tempObject.getString("internal");
//
//                JSONArray jsonSubjectsArray = tempObject.getJSONArray("subjectsList");
//                RealmList<String> subject = new RealmList<>();
//                RealmList<String> points = new RealmList<>();
//
//                for (int j = 0; j < jsonSubjectsArray.length(); j++) {
//                    subject.add(jsonSubjectsArray.getJSONObject(j).getString("subject"));
//                    points.add(jsonSubjectsArray.getJSONObject(j).getString("points"));
//                }
//                majorList.add(new Major(title, university, levelOfEducation, majorForm, budgetaryPlace, specialPlace,
//                        paidPlaces, cost, averagePoint, entrancePoint, subject, points, internal));
//            }
//        } catch (JSONException ignored) {
//        }
//        return majorList;
//    }

    public static void addMajorToRealm(Major major) {

        Realm.getDefaultInstance().executeTransaction(realm -> {
            Major realmObject = realm.createObject(Major.class);

            realmObject.setTitle(major.getTitle());
            realmObject.setLevelOfEducation(major.getLevelOfEducation());
            realmObject.setMajorForm(major.getMajorForm());
            realmObject.setBudgetaryPlace(major.getBudgetaryPlace());
            realmObject.setSpecialPlace(major.getSpecialPlace());
            realmObject.setPaidPlaces(major.getPaidPlaces());
            realmObject.setCost(major.getCost());
            realmObject.setCost(major.getCost());
            realmObject.setCost(major.getCost());
            realmObject.setAveragePoint(major.getAveragePoint());
            realmObject.setEntrancePoint(major.getEntrancePoint());
            realmObject.getSubjects().addAll(major.getSubjects());
            realmObject.setInternal(major.getInternal());
        });
    }
}
