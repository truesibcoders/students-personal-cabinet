package com.truecoders.cabinet.Activity.Entrant;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import com.truecoders.cabinet.Adapter.PagerAdapter.HostelCarouselAdapter;
import com.truecoders.cabinet.Utils.ZoomAnimation;
import com.truecoders.cabinet.R;

/**
 * @author Maria Fabrichkina
 */

public class HostelCarouselViewActivity extends AppCompatActivity {


    private String[] listOfImage;
    private int position;
    private TextView tvCurrent, tvCount;
    private ViewPager viewPager;
    private HostelCarouselAdapter hostelCarouselAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hostel_carousel_view);

        Intent intent = getIntent();
        position = intent.getExtras().getInt("position");
        listOfImage = intent.getStringArrayExtra("images");

        tvCurrent = findViewById(R.id.tvCurrentOfImages);
        tvCount = findViewById(R.id.tvCountOfImagesOnCarousel);
        viewPager = findViewById(R.id.pager);

        tvCount.setText(String.valueOf(listOfImage.length));
        tvCurrent.setText(String.valueOf(position + 1));

        hostelCarouselAdapter = new HostelCarouselAdapter(this, position, listOfImage);

        viewPager.setPageTransformer(true, new ZoomAnimation());
        viewPager.setAdapter(hostelCarouselAdapter);
        viewPager.setCurrentItem(position);
        hostelCarouselAdapter.notifyDataSetChanged();

        viewPager.addOnPageChangeListener(myPageChangeListener);
    }

    ViewPager.OnPageChangeListener myPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            viewPager.getAdapter().notifyDataSetChanged();
            tvCurrent.setText(String.valueOf(position + 1));
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };
}
