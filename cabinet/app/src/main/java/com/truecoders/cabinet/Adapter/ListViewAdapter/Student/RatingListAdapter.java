package com.truecoders.cabinet.Adapter.ListViewAdapter.Student;

import android.view.View;
import android.widget.TextView;
import com.truecoders.cabinet.Model.Student.ECourse;
import com.truecoders.cabinet.Model.Student.Subject;
import com.truecoders.cabinet.R;
import com.truecoders.expandingview.ExpandingItem;
import com.truecoders.expandingview.ExpandingList;
import io.realm.RealmList;

/**
 * @author Ilya Baykalov
 */

public class RatingListAdapter {

    public static void renderRecordBookView(final ExpandingList view, final RealmList<Subject> subjectList) {
        for (Subject subject : subjectList) {
            ExpandingItem item = view.createNewItem(R.layout.expanding_layout);
            if (subject != null) {
                ((TextView) item.findViewById(R.id.expanding_item_title)).setText(subject.getSubjectName());

                item.createSubItems(4);

                final View subItemZero = item.getSubItemView(0);
                ((TextView) subItemZero.findViewById(R.id.label)).setText("Преподаватель");
                if (subject.getTeacher() == null) {
                    ((TextView) subItemZero.findViewById(R.id.value)).setText("Не назначен");

                    subItemZero.findViewById(R.id.value).setOnClickListener(v -> {
                        subItemZero.findViewById(R.id.value).setSelected(!subItemZero.findViewById(R.id.value).isSelected());
                    });
                } else {
                    for (int j = 0; j < subject.getTeacher().size(); j++)
                        ((TextView) subItemZero.findViewById(R.id.value)).setText(subject.getTeacher().get(j));
                }

                View subItemOne = item.getSubItemView(1);
                ((TextView) subItemOne.findViewById(R.id.label)).setText("Кол-во часов");
                ((TextView) subItemOne.findViewById(R.id.value)).setText(subject.getHours() == null ? "-" : subject.getHours());

                View subItemTwo = item.getSubItemView(2);
                ((TextView) subItemTwo.findViewById(R.id.label)).setText("Оценка");
                ((TextView) subItemTwo.findViewById(R.id.value)).setText(subject.getMark() == null ? "-" : subject.getMark());

                View subItemThree = item.getSubItemView(3);
                ((TextView) subItemThree.findViewById(R.id.label)).setText("Дата");
                ((TextView) subItemThree.findViewById(R.id.value)).setText(subject.getMarkDate() == null ? "-" : subject.getMarkDate());

                if (subject.getMark() != null) {
                    item.setIndicatorIconRes(R.drawable.ic_check_solid);
                    item.setIndicatorColorRes(R.color.colorGreenProgress);
                } else {
                    item.setIndicatorIconRes(R.drawable.ic_exclamation);
                    item.setIndicatorColorRes(R.color.colorRedProgress);
                }
            }
        }
    }

    public static void renderProgressView(final ExpandingList view, final RealmList<ECourse> eCoursesLists) {
        for (ECourse eCourse : eCoursesLists) {
            ExpandingItem item = view.createNewItem(R.layout.expanding_layout);
            if (eCourse != null) {
                ((TextView) item.findViewById(R.id.expanding_item_title)).setText(eCourse.getName());

                item.createSubItems(2);

                final View subItemZero = item.getSubItemView(0);
                ((TextView) subItemZero.findViewById(R.id.label)).setText("Оценка");
                ((TextView) subItemZero.findViewById(R.id.value)).setText(eCourse.getMark());
                subItemZero.findViewById(R.id.value).setOnClickListener(v -> subItemZero.findViewById(R.id.value).setSelected(!subItemZero.findViewById(R.id.value).isSelected()));

                View subItemOne = item.getSubItemView(1);
                ((TextView) subItemOne.findViewById(R.id.label)).setText("Диапазон");
                ((TextView) subItemOne.findViewById(R.id.value)).setText(eCourse.getRange());
                subItemOne.findViewById(R.id.value).setOnClickListener(v -> subItemOne.findViewById(R.id.value).setSelected(!subItemOne.findViewById(R.id.value).isSelected()));

//                item.setIndicatorIconRes(R.drawable.ic_check_solid);
                item.setIndicatorColorRes(R.color.colorGreenProgress);
            }
        }
    }
}
