package com.truecoders.cabinet.Utils;

/**
 * @author Ilya Baykalov
 */

public interface Callback {

    void onSuccess(Integer res);

}