package com.truecoders.cabinet.Fragment.Student;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.truecoders.cabinet.R;

/**
 * @author Ilya Baykalov
 */

public class OrderCreateFragment extends Fragment {
    private EditText inputCount;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_set_document, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        Profile profile = Realm.getDefaultInstance().where(Profile.class).findFirst();
//        CheckBox cbStamp = view.findViewById(R.id.cb_stamp);
//        Spinner spinner = view.findViewById(R.id.spinner);
//        inputCount = view.findViewById(R.id.tb_order_count);
//
//        (view.findViewById(R.id.btn_submit)).setOnClickListener(v -> {
//            if (profile != null) {
//                new RetroClient().POST(
//                        "order/",
//                        "{\"login\":\"" + profile.getLogin()
//                                + "\",\"group\":\"" + profile.getGroupName()
//                                + "\",\"count\":" + Integer.parseInt(inputCount.getText().toString())
//                                + ",\"type\":" + (spinner.getSelectedItemPosition() + 1)
//                                + ",\"stamp\":" + cbStamp.isChecked()
//                                + "}",
//                        new Callback() {
//                            @Override
//                            public void onFailure(@NonNull Call call, @NonNull IOException e) {
//                                new OrderTask().execute("Ошибка");
//                            }
//
//                            @Override
//                            public void onResponse(@NonNull Call call, @NonNull Response response) {
//                                new OrderTask().execute("Успешно");
//                            }
//                        });
//            }
//        });
    }

//    @SuppressLint("StaticFieldLeak")
//    private class OrderTask extends AsyncTask<String, Void, String> {
//        @Override
//        protected String doInBackground(String... strings) {
//            return strings[0];
//        }
//
//        protected void onPostExecute(String result) {
//            Toast.makeText(getContext(), result, Toast.LENGTH_SHORT).show();
//        }
//    }
}
