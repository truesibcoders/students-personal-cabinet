package com.truecoders.cabinet.Fragment.Student;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.truecoders.cabinet.Adapter.ListViewAdapter.Student.ScheduleListAdapter;
import com.truecoders.cabinet.Model.General.BaseClass;
import com.truecoders.cabinet.Model.Student.PossibleGroup;
import com.truecoders.cabinet.Model.Student.Profile;
import com.truecoders.cabinet.Model.Student.Schedule;
import com.truecoders.cabinet.R;
import com.truecoders.cabinet.Utils.RetroClient;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.truecoders.cabinet.DAO.Student.ScheduleDAO.addScheduleToRealm;

/**
 * @author Ilya Baykalov
 */

public class ScheduleFragment extends Fragment {
    private ListView scheduleListView;
    private TextView noScheduleLabel;
    private TextView dateTitle;
    private TextView headerTitle;
    private Integer currentDayOfWeek;
    private Integer currentWeek;
    private ProgressBar progressBar;
    private HorizontalCalendar calendar;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_schedule, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Profile profile = Realm.getDefaultInstance().where(Profile.class).findFirst();
        headerTitle = Objects.requireNonNull(getActivity()).findViewById(R.id.header_title);
        dateTitle = view.findViewById(R.id.full_date);

        calendarInit(view);
        getCurrentDate(null);

        currentWeek = Integer.parseInt(loadText());
        headerTitle.setText("Расписание. "
                .concat((currentWeek == 1 ? "Нечётная" : "Чётная")).concat(" неделя"));

        progressBar = view.findViewById(R.id.spin_kit);

        ImageButton btnCalendar = view.findViewById(R.id.btn_calendar);
        btnCalendar.setOnClickListener(showCalendar);
        dateTitle.setOnClickListener(showCalendar);

        scheduleListView = view.findViewById(R.id.list_view_schedule);
        noScheduleLabel = view.findViewById(R.id.no_schedule);

        if (profile != null) {
            if (this.getArguments() != null) {
                if (profile.getGroupLink() == null) {
                    List<PossibleGroup> possibleGroups = (List<PossibleGroup>) this.getArguments().getSerializable("possibleGroups");
                    if (possibleGroups != null) {
                        if (possibleGroups.size() == 1) {
                            Realm.getDefaultInstance().executeTransaction(realm -> {
                                profile.setGroupName(possibleGroups.get(0).getGroup());
                                profile.setGroupLink(possibleGroups.get(0).getLink());
                            });
                        } else if (possibleGroups.size() > 1) {
                            String[] possibleGroupNames = new String[possibleGroups.size()];
                            for (int i = 0; i < possibleGroups.size(); i++) {
                                possibleGroupNames[i] = possibleGroups.get(i).getGroup();
                            }

                            AlertDialog dialog = new AlertDialog.Builder(getContext())
                                    .setTitle("Выберите группу (подгруппу)")
                                    .setItems(possibleGroupNames, (d, pos) -> {
                                        Realm.getDefaultInstance().executeTransaction(realm -> {
                                            profile.setGroupName(possibleGroups.get(pos).getGroup());
                                            profile.setGroupLink(possibleGroups.get(pos).getLink());
                                        });
                                        getSchedule(profile);
                                    }).create();
                            dialog.show();
                        }
                    }
                }
                getSchedule(profile);
            }
        }
    }

    private void getSchedule(Profile profile) {
        if (Realm.getDefaultInstance().where(Schedule.class).count() == 0) {
            RetroClient.getApiService().getSchedule(profile.getGroupLink())
                    .enqueue(new Callback<BaseClass>() {
                        @Override
                        public void onResponse(@NonNull Call<BaseClass> call, @NonNull Response<BaseClass> response) {
                            Realm.getDefaultInstance().executeTransaction(realm -> Realm.getDefaultInstance().delete(Schedule.class));

                            if (response.body() != null) {
                                List<Schedule> scheduleList = response.body().getSchedule();

                                currentWeek = response.body().getWeek();
                                saveText(currentWeek);
                                getCurrentDate(null);

                                for (Schedule schedule : scheduleList) {
                                    addScheduleToRealm(schedule);
                                }
                                showSchedule(currentDayOfWeek, currentWeek);
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<BaseClass> call, @NonNull Throwable t) {

                        }
                    });
        } else {
            showSchedule(currentDayOfWeek, currentWeek);
        }
    }

    private void showSchedule(Integer day, Integer week) {
        List<Schedule> scheduleList = Realm.getDefaultInstance().where(Schedule.class)
                .equalTo("day", day)
                .and()
                .equalTo("week", week)
                .findAll()
                .sort("time");

        if (scheduleList.size() == 0) {
            progressBar.setVisibility(View.GONE);
            scheduleListView.setVisibility(View.INVISIBLE);
            noScheduleLabel.setVisibility(View.VISIBLE);
        } else {
            scheduleListView.setVisibility(View.VISIBLE);
            noScheduleLabel.setVisibility(View.INVISIBLE);
            ScheduleListAdapter adapter = new ScheduleListAdapter(getContext(), scheduleList);
            progressBar.setVisibility(View.GONE);
            scheduleListView.setAdapter(adapter);
        }
    }

    private void getCurrentDate(Date date) {
        currentDayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
        dateTitle.setText(capitalize(new SimpleDateFormat("EEEE, dd MMM", Locale.getDefault())
                .format(date != null ? date : new Date())));
    }

    private void saveText(Integer value) {
        SharedPreferences sPref = Objects.requireNonNull(this.getActivity()).getSharedPreferences("pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString("current_week", String.valueOf(value));
        ed.apply();
    }

    private String loadText() {
        SharedPreferences sPref = Objects.requireNonNull(this.getActivity()).getSharedPreferences("pref", Context.MODE_PRIVATE);
        return sPref.getString("current_week", "0");
    }

    private View.OnClickListener showCalendar = v -> calendar.getCalendarView().setVisibility(calendar.getCalendarView().getVisibility() != View.GONE ? View.GONE : View.VISIBLE);

    private String capitalize(String capString) {
        return capString.substring(0, 1).toUpperCase() +
                capString.substring(1);
    }

    private void calendarInit(View view) {
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -4);

        calendar = new HorizontalCalendar.Builder(view, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .configure()
                .showTopText(false)
                .end()
                .defaultSelectedDate(cal)
                .build();

        calendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                Integer day = date.get(Calendar.DAY_OF_WEEK) - 1;
                Integer currentWeekOfYear = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
                Integer selectedWeek = date.get(Calendar.WEEK_OF_YEAR);

                int week = ((Math.abs(selectedWeek - currentWeekOfYear)) % 2) == 1 ? (currentWeek == 1 ? 2 : 1) : currentWeek;
                headerTitle.setText("Расписание. "
                        .concat((week == 1 ? "Нечётная" : "Чётная")).concat(" неделя"));

                showSchedule(day, week);
                getCurrentDate(date.getTime());
            }
        });
    }
}

