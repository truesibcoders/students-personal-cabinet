let request = require("request");
let cheerio = require("cheerio");

let fs = require("fs");

// Запросы
module.exports = {
    getSchedule: getScheduleBeta,
    getAllGroups: getAllGroups
};

// Получить список групп
function getAllGroups(req, res) {
    let URL = 'http://edu.sfu-kras.ru/timetable';
    let groupNames = [];
    let groupLinks = [];

    request(URL, function (error, response, body) {
        if (!error) {
            let $ = cheerio.load(body, {decodeEntities: false}),
                groups = $(".timetable-groups > ul > li");

            groups.each(function (i, group) {
                let groups = $(group).find("ul > li");
                groups.each(function (i, group) {
                    let groups = $(group).find("ul");
                    if (groups.html() != null) {
                        let names = groups.html().split(/<li><a href="\?group=\S+">([А-я0-9\-(\s)\/.,]+)<\/a><\/li>/);

                        for (i = 0; i < names.length; i++) {
                            let name = names[i].replace(/([А-я0-9]+)\s*-\s*([А-я0-9]+)/, '$1-$2');
                            groupNames.push(name);
                        }
                        for (i = 0; i < groupNames.length; i++) {
                            if (groupNames[i] === '' || groupNames[i] === undefined) {
                                groupNames.splice(i, 1);
                            }
                        }

                        let links = groups.html().split(/<li><a href="\?group=(\S+)">[А-я0-9\-(\s)\/.,]+<\/a><\/li>/)
                        for (i = 0; i < links.length; i++) {
                            groupLinks.push(links[i]);
                        }
                        for (i = 0; i < groupLinks.length; i++) {
                            if (groupLinks[i] === '' || groupLinks[i] === undefined) {
                                groupLinks.splice(i, 1);
                            }
                        }
                    }
                });
            });

            let file = JSON.parse(fs.readFileSync('files/student/groups.json', 'utf-8'));
            for (let i = 0; i < groupNames.length; i++) {
                file.groups.push({"name": groupNames[i], "link": groupLinks[i]});
                fs.writeFileSync('files/student/groups.json', JSON.stringify(file, null, 2));
            }

            res.status(200)
                .json({
                    name: groupNames.length,
                    links: groupLinks.length
                });
        }
    });
}

// Получение расписания
function getSchedule(req, res) {
    /** @namespace req.body.link */
    let URL = 'http://edu.sfu-kras.ru/timetable?group=' + req.body.groupLink;
    // let URL = 'http://edu.sfu-kras.ru/timetable?group=' + encodeURIComponent(req.body.groupName);
    let data = [];
    let currentWeek = '';
    let currentDay = '';
    let twoWeek = false;
    let time = 1;
    let subjectName = [];
    let room = '';
    let teacher = '';
    let subjectType = '';

    request(URL, function (error, response, body) {
        if (!error) {
            let $ = cheerio.load(body),
                rows = $(".table.timetable tr"),
                week = $(".content p b").text();

            week = week.match(/\S+\s(\S+)\s\S+/)[1];
            currentWeek = week === 'нечётная' ? 1 : 2;

            rows.each(function (i, row) {
                if ($(row).hasClass('heading heading-section')) {
                    currentDay = $(row, ".heading.heading-section").text();
                }

                if (!$(row).hasClass('heading')) {

                    if ($(row).hasClass('table-center')) {
                        let columns = $(row).find("tr > td");
                        twoWeek = columns.length !== 3;
                        time = Number($(columns[0]).text());

                        let splitStr = $(columns[2]).text().split(/\s\((\S+)\)((\S+\s\S\.\s\S\.\s*)*)[а-я./]*/);

                        if (splitStr[0] !== undefined) {
                            subjectName.push(splitStr[0]);
                        }
                        if (splitStr[1] !== undefined) {
                            subjectType = splitStr[1];
                        }
                        if (splitStr[2] !== undefined) {
                            teacher = splitStr[2];
                        }
                        if (splitStr[4] !== undefined) {
                            room = splitStr[4];
                        }

                        if (twoWeek) {
                            let splitStr = $(columns[3]).text().split(/\s\((\S+)\)((\S+\s\S\.\s\S\.\s*)*)[а-я./]*/);

                            if (splitStr[0] !== undefined) {
                                subjectName.push(splitStr[0]);
                            }
                            if (splitStr[1] !== undefined) {
                                subjectType = splitStr[1];
                            }
                            if (splitStr[2] !== undefined) {
                                teacher = splitStr[2];
                            }
                            if (splitStr[4] !== undefined) {
                                room = splitStr[4];
                            }
                        }
                    }

                    if (twoWeek) {
                        if (subjectName[0] !== '') {
                            data.push(newSubject(subjectName[0], 1, currentDay, time, room, teacher, subjectType));
                        }
                        if (subjectName[1] !== '') {
                            data.push(newSubject(subjectName[1], 2, currentDay, time, room, teacher, subjectType));
                        }
                    } else {
                        if (subjectName[0] !== '') {
                            data.push(newSubject(subjectName[0], 1, currentDay, time, room, teacher, subjectType));
                            data.push(newSubject(subjectName[0], 2, currentDay, time, room, teacher, subjectType));
                        }
                    }
                    twoWeek = false;
                    time = 1;
                    subjectName = [];
                    room = '';
                    teacher = '';
                    subjectType = '';
                }
            });
        } else {
            console.log("Произошла ошибка: " + error);
        }

        res.status(200)
            .json({
                status: 'success',
                week: currentWeek,
                schedule: data
            });
    });
}

function getScheduleBeta(req, res) {
    res.status(200)
        .json(
            {
                "status": "success",
                "week": 1,
                "schedule": [
                    {
                        "subjectName": "Форматы квалификационных экзаменов по английскому языку",
                        "week": 1,
                        "day": 1,
                        "time": 5,
                        "room": " 43-08",
                        "teacher": "Ерофеева А. А.",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Форматы квалификационных экзаменов по английскому языку",
                        "week": 2,
                        "day": 1,
                        "time": 5,
                        "room": " 43-08",
                        "teacher": "Ерофеева А. А.",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Форматы квалификационных экзаменов по английскому языку",
                        "week": 1,
                        "day": 1,
                        "time": 6,
                        "room": " 43-08",
                        "teacher": "Ерофеева А. А.",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Форматы квалификационных экзаменов по английскому языку",
                        "week": 2,
                        "day": 1,
                        "time": 6,
                        "room": " 43-08",
                        "teacher": "Ерофеева А. А.",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Философия",
                        "week": 1,
                        "day": 2,
                        "time": 3,
                        "room": " 44-09",
                        "teacher": "Сергиенко Р. А.",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Радиационная биофизика",
                        "week": 1,
                        "day": 2,
                        "time": 4,
                        "room": " 13-01",
                        "teacher": "Григорьев А. И.",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Радиационная биофизика",
                        "week": 2,
                        "day": 2,
                        "time": 4,
                        "room": " 13-01",
                        "teacher": "Григорьев А. И.",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Молекулярное моделирование",
                        "week": 1,
                        "day": 2,
                        "time": 5,
                        "room": "13-01",
                        "teacher": "Суковатая И. Е.",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Молекулярное моделирование",
                        "week": 2,
                        "day": 2,
                        "time": 5,
                        "room": "13-01",
                        "teacher": "Суковатая И. Е.",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Биофизика популяций",
                        "week": 1,
                        "day": 3,
                        "time": 1,
                        "room": " 44-08",
                        "teacher": "Рогозин Д. Ю.",
                        "subjectType": true
                    },
                    {
                        "subjectName": "Биофизика популяций",
                        "week": 2,
                        "day": 3,
                        "time": 1,
                        "room": " 44-08",
                        "teacher": "Рогозин Д. Ю.",
                        "subjectType": true
                    },
                    {
                        "subjectName": "Введение в биотехнологию",
                        "week": 1,
                        "day": 3,
                        "time": 2,
                        "room": " 44-09",
                        "teacher": "Сергиенко Р. А.",
                        "subjectType": true
                    },
                    {
                        "subjectName": "Философия",
                        "week": 2,
                        "day": 3,
                        "time": 2,
                        "room": " 44-09",
                        "teacher": "Сергиенко Р. А.",
                        "subjectType": true
                    },
                    {
                        "subjectName": "Фотобиофизика",
                        "week": 1,
                        "day": 3,
                        "time": 3,
                        "room": "13-01",
                        "teacher": "Суковатая И. Е.",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Фотобиофизика",
                        "week": 2,
                        "day": 3,
                        "time": 3,
                        "room": "13-01",
                        "teacher": "Суковатая И. Е.",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Введение в биотехнологию",
                        "week": 1,
                        "day": 3,
                        "time": 4,
                        "room": "Институт биофизики",
                        "teacher": "Волова Т. Г.",
                        "subjectType": true
                    },
                    {
                        "subjectName": "Спецпрактикум (лабораторная работа)Гульнов Д. В.ауд. 13-10",
                        "week": 1,
                        "day": 4,
                        "time": 1,
                        "room": "",
                        "teacher": "",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Спецпрактикум (лабораторная работа)Гульнов Д. В.ауд. 13-10",
                        "week": 2,
                        "day": 4,
                        "time": 1,
                        "room": "",
                        "teacher": "",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Спецпрактикум (лабораторная работа)Гульнов Д. В.ауд. 13-10",
                        "week": 1,
                        "day": 4,
                        "time": 2,
                        "room": "",
                        "teacher": "",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Спецпрактикум (лабораторная работа)Гульнов Д. В.ауд. 13-10",
                        "week": 2,
                        "day": 4,
                        "time": 2,
                        "room": "",
                        "teacher": "",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Большой практикум (лабораторная работа)Немцева Е. В.ауд.13-07",
                        "week": 1,
                        "day": 4,
                        "time": 3,
                        "room": "",
                        "teacher": "",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Большой практикум (лабораторная работа)Немцева Е. В.ауд.13-07",
                        "week": 2,
                        "day": 4,
                        "time": 3,
                        "room": "",
                        "teacher": "",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Большой практикум (лабораторная работа)Немцева Е. В.ауд.13-07",
                        "week": 1,
                        "day": 4,
                        "time": 4,
                        "room": "",
                        "teacher": "",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Большой практикум (лабораторная работа)Немцева Е. В.ауд.13-07",
                        "week": 2,
                        "day": 4,
                        "time": 4,
                        "room": "",
                        "teacher": "",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Биофизика наземных экосистем",
                        "week": 1,
                        "day": 5,
                        "time": 2,
                        "room": " 43-12",
                        "teacher": "Шашкин А. В.",
                        "subjectType": true
                    },
                    {
                        "subjectName": "Биофизика наземных экосистем",
                        "week": 2,
                        "day": 5,
                        "time": 2,
                        "room": " 43-12",
                        "teacher": "Шашкин А. В.",
                        "subjectType": true
                    },
                    {
                        "subjectName": "Биофизика наземных экосистем",
                        "week": 1,
                        "day": 5,
                        "time": 3,
                        "room": " 43-12",
                        "teacher": "Шашкин А. В.",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Большой практикум (лабораторная работа)Немцева Е. В.ауд.43-12",
                        "week": 2,
                        "day": 5,
                        "time": 3,
                        "room": " 43-12",
                        "teacher": "Шашкин А. В.",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Большой практикум (лабораторная работа)Немцева Е. В.ауд.43-12",
                        "week": 1,
                        "day": 5,
                        "time": 4,
                        "room": "",
                        "teacher": "",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Большой практикум (лабораторная работа)Немцева Е. В.ауд.43-12",
                        "week": 2,
                        "day": 5,
                        "time": 4,
                        "room": "",
                        "teacher": "",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Большой практикум (лабораторная работа)Немцева Е. В.ауд.43-12",
                        "week": 1,
                        "day": 5,
                        "time": 5,
                        "room": "",
                        "teacher": "",
                        "subjectType": false
                    },
                    {
                        "subjectName": "Большой практикум (лабораторная работа)Немцева Е. В.ауд.43-12",
                        "week": 2,
                        "day": 5,
                        "time": 5,
                        "room": "",
                        "teacher": "",
                        "subjectType": false
                    }
                ]
            }
        )
}

function newSubject(subjectName, week, currentDay, time, room, teacher, subjectType) {
    let day = 0;
    let type = false;

    subjectName = subjectName[subjectName.length - 1] === ' ' ? subjectName.substr(0, subjectName.length - 1) : subjectName;

    room = room.replace(/,/, '');

    switch (currentDay) {
        case 'Понедельник':
            day = 1;
            break;
        case 'Вторник':
            day = 2;
            break;
        case 'Среда':
            day = 3;
            break;
        case 'Четверг':
            day = 4;
            break;
        case 'Пятница':
            day = 5;
            break;
        case 'Суббота':
            day = 6;
            break;
    }

    switch (subjectType) {
        case 'лекция':
            type = true;
            break;
        case 'практика':
            type = false;
            break;
    }

    return {
        'subjectName': subjectName,
        'week': week,
        'day': day,
        'time': time,
        'room': room === undefined ? '' : room,
        'teacher': teacher === undefined ? '' : teacher,
        'subjectType': type
    };
}
