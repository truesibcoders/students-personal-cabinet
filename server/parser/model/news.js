class News {

    constructor(link, title, imageUrl, category, date, description) {
        this.setLink = link;
        this.setTitle = title;
        this.setImageUrl = imageUrl;
        this.setCategory = category;
        this.setDate = date;
        this.setDescription = description;
    }

    set setLink(newValue) {
        this.link = newValue;
    }

    get getLink(){
        return this.link
    }
    set setTitle(newValue) {
        this.title = newValue;
    }
    set setImageUrl(newValue) {
        this.imageUrl = newValue;
    }
    set setCategory(newValue) {
        this.category = newValue;
    }
    set setDate(newValue) {
        this.date = newValue;
    }
    set setDescription(newValue) {
        this.description = newValue;
    }
}

module.exports = News;