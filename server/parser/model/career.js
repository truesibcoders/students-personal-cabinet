class Career {

    constructor(image, link, title, date, people, status) {
        this.setImage = image;
        this.setLink = link;
        this.setTitle = title;
        this.setDate = date;
        this.setPeople = people;
        this.setStatus = status;
    }

    set setImage(value) {
        this.image = value;
    }

    set setLink(value) {
        this.link = value;
    }

    get getLink() {
        return this.link;
    }

    set setTitle(value) {
        this.title = value;
    }

    set setDate(value) {
        this.date = value;
    }

    set setPeople(value) {
        this.people = value;
    }

    set setStatus(value) {
        this.status = value;
    }
}

module.exports = Career;