class Profile {

    constructor(idStudentCard, idHumanFace, idDicGroup, surname, name, patronymic, institute, groupName, classmatesLink, groupLink, recordBook, email, username, password, birthday, isNotification, avatar) {
        this.setSurname = surname;
        this.setName = name;
        this.setPatronymic = patronymic;
        this.setInstitute = institute;
        this.setGroupName = groupName;
        this.setClassmatesLink = classmatesLink;
        this.setGroupLink = groupLink;
        this.setRecordBook = recordBook;
        this.setEmail = email;
        this.setUsername = username;
        this.setPassword = password;
        this.setBirthday = birthday;
        this.setAvatar = avatar;
    }

    set setSurname(newValue) {
        this.surname = newValue;
    }

    set setName(newValue) {
        this.name = newValue;
    }

    set setPatronymic(newValue) {
        this.patronymic = newValue;
    }

    set setInstitute(newValue) {
        this.institute = newValue;
    }

    set setGroupName(newValue) {
        this.groupName = newValue;
    }

    get getGroupName() {
        return this.groupName;
    }

    set setClassmatesLink(newValue){
        this.classmatesLink = newValue;
    }

    set setGroupLink(newValue) {
        this.groupLink = newValue;
    }

    get getRecordBook() {
        return this.recordBook;
    }

    set setRecordBook(newValue) {
        this.recordBook = newValue;
    }

    set setEmail(newValue) {
        this.email = newValue;
    }

    get getEmail() {
        return this.email;
    }

    set setUsername(newValue) {
        this.username = newValue;
    }

    get getUsername() {
        return this.username;
    }

    set setPassword(newValue) {
        this.password = newValue;
    }

    get getPassword() {
        return this.password;
    }

    set setBirthday(newValue) {
        this.birthday = newValue;
    }

    get getAvatar() {
        return this.avatar;
    }

    set setAvatar(newValue) {
        this.avatar = newValue;
    }
}

module.exports = Profile;
