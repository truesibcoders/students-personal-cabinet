class Semester {

    constructor() {
        this.semesterList = [];
    }

    get getSemesterList() {
        return this.semesterList;
    }
}

module.exports = Semester;
