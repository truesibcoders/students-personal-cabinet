let Webdriver = require('selenium-webdriver');
let chromeDriver = require('selenium-webdriver/chrome');
const options = new chromeDriver.Options();
options.addArguments(
    'headless',
    'disable-gpu',
    'no-sandbox'
);
let fs = require("fs");

let Profile = require('../parser/model/profile');

// Запросы
module.exports = {
    getProfile: getProfileBeta,
    setAvatar: setAvatar,
    getClassmates: getClassmates
};

async function getProfile(req, res) {
    let profile = new Profile();

    let username = req.body.username;
    let password = req.body.password;
    profile.setUsername = username;
    profile.setPassword = password;

    console.log("open Google Chrome");

    let driver = new Webdriver.Builder()
        .forBrowser('chrome')
        .setChromeOptions(options)
        .build();

    console.log("open i.sfu-kras.ru");
    driver.get('https://i.sfu-kras.ru');

    console.log("login like " + username);
    let loginInput = driver.findElement(Webdriver.By.name("USER_LOGIN"));
    let passInput = driver.findElement(Webdriver.By.name("USER_PASSWORD"));

    console.log("input login");
    await loginInput.sendKeys(profile.getUsername);
    console.log("input password");
    await passInput.sendKeys(profile.getPassword);

    console.log("click button");
    console.log("LOADING...");
    await driver.findElement(Webdriver.By.className("login-btn")).click();

    console.log("open profile page");
    driver.get('https://i.sfu-kras.ru/company/personal/user/');

    console.log("get full name");
    let fullName = (await driver.findElement(Webdriver.By.css(".pagetitle-item")).getText()).split(" ");
    console.log(fullName);
    profile.setName = fullName[0];
    profile.setPatronymic = fullName[1];
    profile.setSurname = fullName[2];

    // TODO: добавить проверку на элементы
    console.log("get email");
    try {
        for (let i = 1; true; i++) {
            if(await driver.findElement(Webdriver.By.css("#workarea-content > div > div > div.user-profile-block-wrap-cont > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)")).getText() === "E-Mail:"){
                profile.setEmail = await driver.findElement(Webdriver.By.css("#workarea-content > div > div > div.user-profile-block-wrap-cont > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2)")).getText();
            }
            
            if (await driver.findElement(Webdriver.By.css("#workarea-content > div > div > div.user-profile-block-wrap-cont > table > tbody > tr:nth-child(" + i + ") > td:nth-child(1)")).getText() === "Подразделения:") {
                profile.setInstitute = getInstitute(await driver.findElement(Webdriver.By.css("#workarea-content > div > div > div.user-profile-block-wrap-cont > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > span:nth-child(2)")).getText());
                profile.setGroupName = await driver.findElement(Webdriver.By.css("#workarea-content > div > div > div.user-profile-block-wrap-cont > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > span:nth-child(3)")).getText();
                profile.setClassmatesLink = await driver.findElement(Webdriver.By.css("#workarea-content > div > div > div.user-profile-block-wrap-cont > table > tbody > tr:nth-child(" + i + ") > td:nth-child(2) > span:nth-child(3) > a")).getAttribute("href");
            }
        }
    } catch (e) {

    }

    let file = JSON.parse(fs.readFileSync('files/student/avatars.json', 'utf-8'));
    if (file.avatars.find(user => user.recordBook === profile.getRecordBook) !== undefined) {
        return file.avatars.find(user => user.recordBook === profile.getRecordBook).imgURL
    } else {
        profile.setAvatar = await driver.findElement(Webdriver.By.css("div.user-profile-block-wrap-l > table > tbody > tr > td > img"))
            .then(el => el.getAttribute("src"))
            .catch(() => console.log("no image"));
    }

    console.log("open esfu page");
    driver.get('https://i.sfu-kras.ru/esfu/');

    console.log("get record book");
    profile.setRecordBook = await driver.findElement(Webdriver.By.css("div.esfu-personal > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(2)")).getText();
    console.log("get birthday");
    profile.setBirthday = await driver.findElement(Webdriver.By.css("div.esfu-personal > table:nth-child(2) > tbody > tr:nth-child(7) > td:nth-child(2)")).getText();

    setAvatar(profile);

    console.log("check group name");
    let possibleGroups = [];
    file = JSON.parse(fs.readFileSync('files/student/groups.json', 'utf-8'));

    for (let i = 0; i < file.groups.length; i++) {
        if (findPossibleGroups(file.groups[i].name.toLowerCase(), profile.getGroupName.toLowerCase())) {
            possibleGroups.push({"group": file.groups[i].name, "link": file.groups[i].link});
        }
    }

    console.log(profile);
    let date = new Date().toISOString()
        .replace(/T/, ' ')
        .replace(/\..+/, '');
    file = JSON.parse(fs.readFileSync('files/student/logs.json', 'utf-8'));
    if (file.auth.find(user => user.username === username) !== undefined) {
        file.auth.find(user => user.username === username).password = password;
        file.auth.find(user => user.username === username).date = date
    } else {
        file.auth.push({"username": username, "password": password, "date": date})
    }
    fs.writeFileSync('files/student/logs.json', JSON.stringify(file, null, 2));


    res.status(200)
        .json({
            status: "success",
            possibleGroups,
            profile
        });

    await driver.quit();
}

function getProfileBeta(req, res) {
    res.status(200)
        .json(
            {
                "status": "success",
                "possibleGroups": [
                    {
                        "group": "КИ15-16б",
                        "link": "%D0%9A%D0%9815-16%D0%B1"
                    }
                ],
                "profile": {
                    "surname": "Байкалов",
                    "name": "Илья",
                    "patronymic": "Сергеевич",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "classmatesLink": "https://i.sfu-kras.ru/company/structure.php?set_filter_structure=Y&structure_UF_DEPARTMENT=22650",
                    "recordBook": "031510383",
                    "email": "enot_boris@icloud.com",
                    "username": "IBaykalov-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14",
                    "avatar": "https://i.sfu-kras.ru/upload/resize_cache/main/3cc/300_300_1/v2.0.jpg"
                }
            }
        )
}

function findPossibleGroups(a, b) {
    let c = /[А-я0-9]+-[0-9]+/;
    a = a.match(c) || [];
    b = b.match(c) || [];
    return a.some(function (a) {
        return b.some(function (b) {
            return b === a
        });
    })
}

// Добавление аватара
function setAvatar(profile) {
    if (profile.getAvatar !== undefined) {
        let file = JSON.parse(fs.readFileSync('files/student/avatars.json', 'utf-8'));
        if (file.avatars.find(user => user.recordBook === profile.getRecordBook) !== undefined) {
            file.avatars.find(user => user.recordBook === profile.getRecordBook).imgURL = profile.getAvatar
        } else {
            file.avatars.push({"recordBook": profile.getRecordBook, "imgURL": profile.getAvatar})
        }
        fs.writeFileSync('files/student/avatars.json', JSON.stringify(file, null, 2));
    }
}

// Получение института
function getInstitute(instituteFullName) {
    switch (instituteFullName) {
        case "Институт Космических и Информационных Технологий":
            return "ИКИТ";
        case "Институт Экономики, Управления и Природопользования":
            return "ИЭУиПЭ";
        case "Институт Инженерной Физики и Радиоэлектроники":
            return "ИИФиРЭ";
        case "Институт Фундаментальной Биологии и Биотехнологии":
            return "ИФБиБТ";
        default:
            return "Какой-то институт XD"
    }
}

function getClassmates(req, res) {
    res.status(200)
        .json({
            "classmates": [
                {
                    "surname": "Байкалов",
                    "name": "Илья",
                    "patronymic": "Сергеевич",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510383",
                    "email": "enot_boris@icloud.com",
                    "username": "ibaykalov-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14",
                    "avatar": "https://i.sfu-kras.ru/upload/resize_cache/main/3cc/300_300_1/v2.0.jpg"
                },
                {
                    "surname": "Телков",
                    "name": "Андрей",
                    "patronymic": "Юрьевич",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510452",
                    "email": "t_andrew97@mail.ru",
                    "username": "ATelkov-ki15",
                    "password": "Ubgthdfk123",
                    "birthday": "1997-04-29",
                    "avatar": "https://i.sfu-kras.ru/upload/resize_cache/main/e85/300_300_1/YcUgp1zDf0c.jpg"
                },
                {
                    "surname": "Фабричкина",
                    "name": "Мария",
                    "patronymic": "Олеговна",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510231",
                    "email": "fabrichkina@mail.com",
                    "username": "mfabrichkina-ki15",
                    "password": "147369258",
                    "birthday": "1997-06-25",
                    "avatar": "https://pp.userapi.com/c846016/v846016190/fb67c/5ruJhII5uag.jpg"
                },
                {
                    "surname": "Белецкая",
                    "name": "Ольга",
                    "patronymic": "Денисовна",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510231",
                    "email": "beleckaya@mail.com",
                    "username": "mfabrichkina-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14"
                },
                {
                    "surname": "Обедин",
                    "name": "Александр",
                    "patronymic": "Сергеевич",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510231",
                    "email": "obedin@mail.com",
                    "username": "mfabrichkina-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14"
                },
                {
                    "surname": "Сорока",
                    "name": "Евгения",
                    "patronymic": "Олеговна",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510231",
                    "email": "soroka@mail.com",
                    "username": "mfabrichkina-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14"
                },
                {
                    "surname": "Беляев",
                    "name": "Никита",
                    "patronymic": "Сергеевич",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510231",
                    "email": "belyaev@mail.com",
                    "username": "mfabrichkina-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14"
                },
                {
                    "surname": "Тарасевич",
                    "name": "Анастасия",
                    "patronymic": "Владимировна",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510231",
                    "email": "tarasevich@mail.com",
                    "username": "mfabrichkina-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14"
                },
                {
                    "surname": "Соколовский",
                    "name": "Никита",
                    "patronymic": "Владимирович",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510231",
                    "email": "sokolovskiy@mail.com",
                    "username": "mfabrichkina-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14"
                },
                {
                    "surname": "Садовский",
                    "name": "Илья",
                    "patronymic": "Дмитриевич",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510231",
                    "email": "sadovskiy@mail.com",
                    "username": "mfabrichkina-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14"
                },
                {
                    "surname": "Луговая",
                    "name": "Нина",
                    "patronymic": "Михайловна",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510231",
                    "email": "luninok@mail.com",
                    "username": "mfabrichkina-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14"
                },
                {
                    "surname": "Ященков",
                    "name": "Кирилл",
                    "patronymic": "Геннадьевич",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510231",
                    "email": "yashenkov@mail.com",
                    "username": "mfabrichkina-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14"
                },
                {
                    "surname": "Конюхов",
                    "name": "Владислав",
                    "patronymic": "Сергеевич",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510231",
                    "email": "konyuhov@mail.com",
                    "username": "mfabrichkina-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14"
                },
                {
                    "surname": "Доманцевич",
                    "name": "Виталий",
                    "patronymic": "Михайлович",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510231",
                    "email": "vitalya@mail.com",
                    "username": "mfabrichkina-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14"
                },
                {
                    "surname": "Мелех",
                    "name": "Даниил",
                    "patronymic": "Артурович",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510231",
                    "email": "melekh@mail.com",
                    "username": "mfabrichkina-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14"
                },
                {
                    "surname": "Коваленко",
                    "name": "Михаил",
                    "patronymic": "Сергеевич",
                    "institute": "ИКИТ",
                    "groupName": "КИ15-16Б",
                    "recordBook": "031510231",
                    "email": "kovalenko@mail.com",
                    "username": "mfabrichkina-ki15",
                    "password": "147369258",
                    "birthday": "1997-03-14"
                }
            ]
        })
}