let request = require("request");
let cheerio = require("cheerio");

let fs = require("fs");

let News = require('../parser/model/news');

module.exports = {
    getNews: getNews
};

async function getNews(req, res) {

    let institute = req.body.institute;
    //let institute = "ИЭУиПЭ";

    checkInstitute(institute, res);
}

function checkInstitute(institute, res) {
    switch (institute) {
        case "СФУ":
            return getSFUNews(res);
        case "ИКИТ":
            return getIkitNews(res);
        case "ИЭУиПЭ":
            return getIeuipeNews(res);
        case "ИИФиРЭ":
            return getIifireNews(res);
        case "ИФБиБТ":
            return getIfbibtNews(res);
        default:
            break;
    }
}

function getSFUNews(res) {
    let URL = 'http://news.sfu-kras.ru';
    let news = [];

    request(URL, function (error, response, body) {
        if (!error) {
            let $ = cheerio.load(body, {decodeEntities: false}),
                teasers = $(".news-items article");

            teasers.each(function (i) {
                let article = new News();
                article.setLink = "http://news.sfu-kras.ru" + $("#news > div > article:nth-child(" + (i + 1) + ") > a").attr("href");
                article.setCategory = $("#news > div > article:nth-child(" + (i + 1) + ") > a > span.news-item-category").text();
                article.setDate = $("#news > div > article:nth-child(" + (i + 1) + ") > a > time").text();
                article.setTitle = $("#news > div > article:nth-child(" + (i + 1) + ") > a > h3").text();
                article.setDescription = $("#news > div > article:nth-child(" + (i + 1) + ") > div p").text();
                article.setImageUrl = "http://news.sfu-kras.ru" + $("#news > div > article:nth-child(" + (i + 1) + ") > a > span.news-item-image > img").attr("src");

                news.push(article);
            })
        }
        res.status(200)
            .json({
                news
            });
    });
}

function getIkitNews(res) {
    let URL = 'http://ikit.sfu-kras.ru/';
    let news = [];

    request(URL, function (error, response, body) {
        if (!error) {
            let $ = cheerio.load(body, {decodeEntities: false}),
                teasers = $(".indent.clear");

            teasers.each(function (i) {
                let article = new News();
                article.setLink = "http://ikit.sfu-kras.ru" + $(".lenta > div:nth-child(" + (i + 1) + ") > div > div > a").attr("href");
                article.setDate = $(".lenta > div:nth-child(" + (i + 1) + ") > div > div:nth-child(2) > span").text();
                article.setTitle = $(".lenta > div:nth-child(" + (i + 1) + ") > div > div:nth-child(2) > h3").text();
                article.setDescription = $(".lenta > div:nth-child(" + (i + 1) + ") > div > div:nth-child(2) > div > div > div > p").text();
                article.setImageUrl = "http://ikit.sfu-kras.ru" + $(".lenta > div:nth-child(" + (i + 1) + ") > div > div > a > img").attr("src");

                news.push(article);
            })
        }
        res.status(200)
            .json({
                news
            });
    });
}

function getIifireNews(res) {
    let URL = 'http://efir.sfu-kras.ru/services/';
    let news = [];

    request(URL, function (error, response, body) {
        if (!error) {
            let $ = cheerio.load(body, {decodeEntities: false}),
                teasers = $(".services.type-services");

            teasers.each(function (i) {
                let article = new News();
                article.setLink = $(".kopa-article-list > li:nth-child(" + (i + 1) + ") > article > div:nth-child(2) > a").attr("href");
                article.setDate = $(".kopa-article-list > li:nth-child(" + (i + 1) + ") > article > div:nth-child(2) > header > span.entry-date").text();
                article.setTitle = $(".kopa-article-list > li:nth-child(" + (i + 1) + ") > article > div:nth-child(2) > header > h6 > a").text();
                article.setDescription = $(".kopa-article-list > li:nth-child(" + (i + 1) + ") > article > div:nth-child(2) > p").text();
                article.setImageUrl = $(".kopa-article-list > li:nth-child(" + (i + 1) + ") > article > div:nth-child(1) > img").attr("src");

                news.push(article);
            })
        }
        res.status(200)
            .json({
                news
            });
    });
}

function getIfbibtNews(res) {
    let URL = 'http://bio.sfu-kras.ru/';
    let news = [];

    request(URL, function (error, response, body) {
        if (!error) {
            let $ = cheerio.load(body, {decodeEntities: false}),
                articles = $('#news > div');
            for (let i = 0; i <= articles.length; i += 2) {
                let atricle = new News();
                if ($(articles.get(i)).hasClass('date')) {
                    atricle.setDate = $(articles.get(i)).text();
                    i++;
                }
                atricle.setTitle = $(articles.get(i)).text();
                atricle.setLink = "http://bio.sfu-kras.ru" + $("#news > div:nth-child(" + (i + 1) + ") > a").attr('href');
                atricle.setDescription = $(articles.get(i + 1)).text();
                news.push(atricle);
            }

            res.status(200)
                .json({
                    news
                });
        }
    });
}

function getIeuipeNews(res) {
    let URL = 'http://eco.sfu-kras.ru/';

    request(URL, function (error, response, body) {
        if (!error) {
            let news = [];
            let $ = cheerio.load(body, {decodeEntities: false}),
                articles = $('.node.teaser.story');
            articles.each(function (i) {
                let article = new News();

                article.setLink = "http://eco.sfu-kras.ru" + $(".node.teaser.story:nth-child(" + (i + 1) + ") > a").attr("href");
                article.setImageUrl = $(".node.teaser.story:nth-child(" + (i + 1) + ") > a > img").attr("src");
                article.setTitle = $(".node.teaser.story:nth-child(" + (i + 1) + ") > h3 > a").text();//#node-1755 > h3 > a
                article.setDate = $(".node.teaser.story:nth-child(" + (i + 1) + ") > span").text();
                article.setDescription = $(".node.teaser.story:nth-child(" + (i + 1) + ") > div > div > div > div > p").text();

                news.push(article);
            });
            res.status(200)
                .json({
                    news
                });
        }
    })
}