const fs = require("fs");

// Запросы
module.exports = {
    getInstitutes:getInstitutes,
    getContacts: getContacts,
    getHostels: getHostels,
    getMarkers: getMarkers,
    getMajors: getMajors
};

// Получение списка институтов
function getInstitutes(req, res) {
    let data = JSON.parse(fs.readFileSync("files/entrant/institutes.json", "utf-8"));
    res.status(200)
        .json({
            status: "success",
            institutes: data
        })
}

// Получение списка контактов
function getContacts(req, res) {
    let data = JSON.parse(fs.readFileSync("files/entrant/contacts.json", "utf-8"));
    res.status(200)
        .json({
            status: "success",
            contacts: data
        })
}

// Получение списка общежитий
function getHostels(req, res) {
    let data = JSON.parse(fs.readFileSync("files/entrant/hostels.json", "utf-8"));
    res.status(200)
        .json({
            status: "success",
            hostels: data
        })
}

// Получение списка маркеров карты
function getMarkers(req, res) {
    let data = JSON.parse(fs.readFileSync("files/entrant/markers.json", "utf-8"));
    res.status(200)
        .json({
            status: "success",
            markers: data
        })
}

// Получение списка специальностей
function getMajors(req, res) {
    let data = JSON.parse(fs.readFileSync("files/entrant/majors.json", "utf-8"));
    res.status(200)
        .json({
            status: "success",
            majors: data
        })
}
