let config = require('../config/config');

const db = config.db;
let dateFormat = require('dateformat');

// Запросы
module.exports = {
    addOrder: addOrder,
    getOrder: getOrder,
    deleteOrder: deleteOrder
};

// Получение истории справок
function getOrder(req, res, next) {
    let login = req.body.login;
    db.any(
        'SELECT ' +
        'id_fact_sheet,' +
        'id_fact_sheet_status,' +
        'register_number,' +
        'id_type_fact_sheet,' +
        'official_seal,' +
        'date_create,' +
        'is_receipt,' +
        'date_receipt ' +
        'FROM fact_sheet ' +
        'WHERE id_humanface = (SELECT id_humanface FROM studentcard sc ' +
        'WHERE sc.ldap_login ~* $1 ' +
        'LIMIT 1) ORDER BY id_fact_sheet DESC ', login)
        .then(function (data) {
            for (let i = 0; i < data.length; i++) {
                data[i].date_create = dateFormat(data[i].date_create, "yyyy-mm-dd h:MM:ss");
                data[i].date_receipt = data[i].date_receipt == null ? null : dateFormat(data[i].date_receipt, "yyyy-mm-dd h:MM:ss")
            }
            res.status(200)
                .json({
                    status: 'success',
                    data: data
                })
        })
        .catch(function (err) {
            return next(err)
        })
}

// создание заявки на заказ справки
function addOrder(req, res, next) {
    let login = req.body.login;
    let group = req.body.group;
    let count = req.body.count;
    let type = req.body.type;
    /** @namespace req.body.stamp */
    let stamp = req.body.stamp;

    console.log(req.body);
    let str = "";
    for (let i = 0; i < count; i++) {
        str += '((SELECT id_humanface FROM studentcard sc ' +
            'WHERE sc.ldap_login = $3 ' +
            'LIMIT 1),' +
            '$1,' +
            '0,' +
            '0,' +
            '$2,' +
            '$4),'
    }
    str = str.substring(0, str.length - 1);

    db.none('INSERT INTO fact_sheet ' +
        '(id_humanface,' +
        'id_type_fact_sheet,' +
        'id_fact_sheet_status,' +
        'register_number,' +
        'official_seal,' +
        'groupname)' +
        'VALUES' + str, [type, stamp, login, group])
        .then(function () {
            res.status(200)
                .json({
                    status: 'success'
                });
        })
        .catch(function (err) {
            console.log([type, stamp, login, group]);
            return next(err);
        });

}

function deleteOrder(req, res, next) {
    let orderId = parseInt(req.body.id);
    db.result('DELETE FROM fact_sheet WHERE id_fact_sheet = $1', orderId)
        .then(function () {
            res.status(200)
                .json({
                    status: 'success'
                });
        })
        .catch(function (err) {
            return next(err);
        });
}