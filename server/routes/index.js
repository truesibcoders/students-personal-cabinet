let router = require('express').Router();

// index
router.get('/', function (req, res) {
    res.render('index', {title: 'Student\'s cabinet server'})
});
router.get('/favicon.ico', function (req, res) {
});

// queries
let rating = require('../parser/ratingQuery'),
    schedule = require('../parser/scheduleQuery'),
    order = require('../queries/order'),
    entrant = require('../queries/entrant'),
    profileQuery = require('../parser/profileQuery'),
    news = require('../parser/newsQuery'),
    career = require('../parser/careerQuery');

// student
// router.post('/api/student/get', profile.getAuthProfile); // LDAP
router.post('/api/student/get', profileQuery.getProfile);
router.post('/api/profile/avatar/set', profileQuery.setAvatar);
// TODO test
router.post('/api/classmates/get', profileQuery.getClassmates);

// progress
router.post('/api/rating/recordbook/get', rating.getRecordBook);
router.post('/api/rating/progress/get', rating.getProgress);
// router.post('/api/semesters/get', rating.getSemesters);
// router.post('/api/rating/get', rating.getRating);

// schedule
router.post('/api/schedule/get', schedule.getSchedule);
router.post('/api/groups/get', schedule.getAllGroups);

//news
router.post('/api/news/get', news.getNews);

//career
router.post('/api/career/get', career.getCareer);

// order
// router.post('/api/order/get', order.getOrder);
// router.post('/api/order/add', order.addOrder);
// router.post('/api/order/delete', order.deleteOrder);

// entrant
router.post('/api/entrant/contacts/get', entrant.getContacts);
router.post('/api/entrant/hostels/get', entrant.getHostels);
router.post('/api/entrant/markers/get', entrant.getMarkers);
router.post('/api/entrant/institutes/get', entrant.getInstitutes);
router.post('/api/entrant/majors/get', entrant.getMajors);

module.exports = router;
